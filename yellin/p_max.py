from __future__ import print_function, division
import numpy as np
from scipy import special as sp
from scipy.interpolate import interp1d
import os

PathToThisFile = os.path.split(__file__)[0]

pMax = dict(np.load(os.path.join(PathToThisFile,'p_max_90_contour.npz')))
pt_23 = -np.log(.1)
f_interp = interp1d(pMax['dPoints'][:,0],pMax['dPoints'][:,1])


def calc_p_max(mu, dataVals):
    """
    Returns the max p value over all unique intervals in the data.  Assumes that
    input 'dataVals' has been already scaled so that it is from a uniform 
    distribution between zero and one.
    
    Inputs: ------------------------------------------------------------------------
             mu: Expected number of events given the signal (plus potentially known
                 backgrounds).
       dataVals: Value of the data after they have been scaled to be uniform between 
                 zero and one.  This should be a numpy array.
    Outputs: -----------------------------------------------------------------------
          p_max: The maximum of the p_values for each unique interval in dataVals
    
    2014.09.10
    2014.09.18 -- Changed the output to be the maximum p_value, instead of every 
                  p_value (and changed the name of the function to reflect this).
    2018.10.02 -- Modified it so that the function can handle an array of mu.
    """
    flag_mu_array = isinstance(mu, np.ndarray)
    nHit = len(dataVals)
    rvls = dataVals.copy()
    rvls.sort()
    rvls = np.r_[0,rvls,1]
    numIntervals = int((nHit + 1) * (nHit + 2) / 2) # number of intervals
    p_vals = np.empty(numIntervals) # initialize the array
    #k_int = 0 # the iterator for the intervals
    # calculate the boundaries of the intervals:
    itvls = np.array([(rvls[k],rvls[m]) for k in range(len(rvls)-1) for m in range(k+1,len(rvls))])
    # calculate the differences of all intervals:
    d_itvls = np.diff(itvls, axis=1)
    # calculate the expected number of events in each interval:
    mu_itvls = np.dot(np.c_[mu], d_itvls.T)    
    # calculate the observed number of events in each interval
    nObs_itvls = np.array([m-k-1 for k in range(len(rvls)-1) for m in range(k+1,len(rvls))])
    p_vals = sp.pdtrc(nObs_itvls, mu_itvls)
    p_vals_max = p_vals.max(1)
    return p_vals_max if flag_mu_array else p_vals_max[0]


def scaleDataToUniformInterval(dataVals, distCDF_x, distCDF_y):
    """
    Take some input data along some variable, and rescale them so that they would be distributed
    uniformly (from zero to one) if they came from the probability distribution described by the
    second two inputs.  The desired distribution is given as a CDF, and the data values are 
    linearly interpolated.
    
    Inputs: ------------------------------------------------------------------------
       dataVals: Value of the data points.
      distCDF_x: The x-values (probably in energy), in the search range, of the 
                 signal PDF.  Naturally, some optimization needs to be kept in mind:
                 Finer spacing yields more accurate results, coarser spacing yields
                 quicker results.
      distCDF_y: The signal CDF, evaluated at the values in distCDF_x. This input 
                 need not be strictly a normalized CDF.
    Outputs: -----------------------------------------------------------------------
     dataScaled: The data from the experiment, scaled in such a way that the input 
                 distribution would be scaled to be a standard uniform distribution.
    
    2015.05.25
    """
    # --- Input handling --- #
    assert dataVals.min() >= distCDF_x.min(), \
        "Input 'dataVals' is not contained within the distribution boundaries."
    assert dataVals.max() <= distCDF_x.max(), \
        "Input 'dataVals' is not contained within the distribution boundaries."
    assert type(dataVals) == np.ndarray, \
        "Input 'dataVals' must be a numpy array."
    assert type(distCDF_x) == np.ndarray, \
        "Input 'distCDF_x' must be a numpy array."
    assert type(distCDF_y) == np.ndarray, \
        "Input 'distCDF_y' must be a numpy array."
    assert len(dataVals.shape) == 1, \
        "Input 'dataVals' must be 1-D."
    assert len(distCDF_x.shape) == 1, \
        "Input 'distCDF_x' must be 1-D."
    assert len(distCDF_y.shape) == 1, \
        "Input 'distCDF_y' must be 1-D."
    assert len(distCDF_x) == len(distCDF_y), \
        "Inputs 'distCDF_x' and 'distCDF_y' must be the same length."
    # --- /Input handling --- #
    
    cdfInterp = interp1d(distCDF_x,distCDF_y/distCDF_y[-1])
    dataScaled = cdfInterp(dataVals)
    return dataScaled

def p_max_90(mu):
    """
    Take as input an expected number of events and return the upper bound on the 90%
    confidence interval on p_max.
    
    The 90'th percentile for this test statistic is undefined for mu <~ 2.3 (or more
    precisely, for mu < -log(0.1)); therefore, this function returns NaN for any value
    of mu for which p_max90 is undefined.
    
    2016.02.26
    """
    if isinstance(mu,float) or isinstance(mu,int):
        if mu < pt_23:
            return 0.9
        elif mu < 15:
            return f_interp(mu)
        elif mu < 40:
            outVal = 1-10**np.polyval(pMax['p_15_quad'],np.log10(mu))
            return outVal
        else:
            outVal = 1-10**np.polyval(pMax['p_40_lin'],np.log10(mu))
            return outVal
    elif isinstance(mu,np.ndarray):
        cut_23 = mu < pt_23
        cut_mid = (mu >= pt_23) & (mu < 15.)
        cut_upper = (mu >= 15.) & (mu < 40.)
        cut_high = mu >= 40.
        outVect = np.empty_like(mu)
        outVect[cut_23]    = 0.9
        outVect[cut_mid]   = f_interp(mu[cut_mid])
        outVect[cut_upper] = 1-10**np.polyval(pMax['p_15_quad'],np.log10(mu[cut_upper]))
        outVect[cut_high]  = 1-10**np.polyval(pMax['p_40_lin'],np.log10(mu[cut_high]))
        return outVect
    else:
        raise TypeError("Cannot interpret input type")



def _points2Eq(p1,p2):
    """
    Takes two points (p1 and p2) that are each an ordered pair (x and y) and
    returns the slope and y-intercept (m and b from y=mx+b) of the line that
    connects the two points.  The x-value of p1 need not be less than that 
    of p2.
    
    2015.06.02
    """
    if p2[0] > p1[0]:
        m = (p2[1]-p1[1])/(p2[0]-p1[0])
        b = p1[1]-m*p1[0]
    else:
        m = (p1[1]-p2[1])/(p1[0]-p2[0])
        b = p1[1]-m*p1[0]
    return m, b

def _x0fromLine(m,b):
    """
    Given the slope (m) and y-intercept (b) of a line (i.e. y = m*x + b), 
    returns the value of x at which the line crosses the x axis.
    
    2015.06.02
    """
    return (-b/m)

def root_search(fun, x_l, x_r, b, *args, alg='regula', tol_b=1e-10, tol_x=1e-10, 
    max_iter=100, print_n=False, **kwargs):
    """
    Root finding, with either the "regula falsi" or "bisection" algorithms. This 
    is useful if we have a function, f(x), and we wish to approximate the value of 
    x at which f(x) crosses some threshold b.  We start by assuming we have 
    x-values 'x_l' and 'x_r' which bracket the true value of x=x_b, at which 
    f(x_b) = b.  The regula falsi algorithm works by drawing the line given from 
    point [x_l,f(x_l)] to point [x_r,f(x_r)], and then finding the value of x_0 where 
    that line crosses b.  The bisection method is simpler, in which x_0 is simply 
    taken as the midpoint between x_l and x_r.  Once x_0 is selected, the two 
    algorithms are the same.  If f(x_0) is on the same side of b as f(x_l), then the 
    value of x_l is replaced by x_0; otherwise, the value of x_r is replaced by 
    x_0.  The process is then repeated indefinitely, to any desired precision.  
    Either it stops when abs(b-f(x_0)) falls below some value, or abs(x_r-x_l) 
    falls below some value.  This works well if f(x) has a step-like discontinuity 
    and we wish to find where that discontinuity occurs.
    
    Additionally, b can be a function, and one is searching for the point at which
    'b' and 'fun' cross.
    
    Very roughly speaking, if x_r-x_l is of the same order as x_b, then the 
    number of iterations is similar to the number of significant digits of 
    precision of the estimate.
    
    Inputs:
           fun: The function to be evaluated
           x_l: The left edge of the initial bracketing window.
           x_r: The right edge of the initial bracketing window.
             b: The value of the function that is to be interpolated.  This input 
                can be a function, in which case the algorithm finds the crossing
                point.  If 'b' is a function, type input must be 'bisection'
         *args: Any additional arguments that are needed by the function
           alg: The algorithm of root finding to be used.  Choices are 'regula' 
                and 'bisection'.
         tol_b: When |b-f(x_0)| falls below this value, the iterations stop.
         tol_x: When |x_r-x_l| falls below this value, the iterations stop.\
      max_iter: Maximum allowed number of iterations
      **kwargs: Any additional keyword arguments needed by the function.
    Outputs:
           x_b: The estimate of the value of x for which f(x_b) = b (if b is a number)
                or for which f(x_b) = b(x_b) (if b is a function)
    
    2018.09.15
    2018.10.04 -- Added the possibility to give input 'b' as  function.  Also allowing 
                  the user to chose the algorithm to use.
    """
    #-------<input checking>-------#
    assert hasattr(fun, '__call__'), "Input 'fun' must be callable."
    assert isinstance(x_l, int) or isinstance(x_l, float), "x_l must be just a number"
    assert isinstance(x_r, int) or isinstance(x_r, float), "x_r must be just a number"
    assert isinstance(b, int) or isinstance(b, float) or hasattr(b,'__call__'), \
        "Input 'b' must be a number or a function"
    assert isinstance(max_iter, int), "max_iter must be an integer"
    if hasattr(b, '__call__'):
        flag_b_func = True
    else:
        flag_b_func = False
    if flag_b_func:
        assert (fun(x_l,*args,**kwargs)-b(x_l))*(fun(x_r,*args,**kwargs)-b(x_r)) < 0., \
            "f(x_l) and f(x_r) must be on opposite sides of b."
    else:
        assert (fun(x_l,*args,**kwargs)-b) * (fun(x_r,*args,**kwargs)-b) < 0., \
            "f(x_l) and f(x_r) must be on opposite sides of b."
    assert alg in ['regula','bisection'], "Input 'alg' must be either 'regula' or 'bisection'"
    if flag_b_func and alg == 'regula':
        print("Warning: 'regula' method cannot be used if b is a function. Switching to bisection.")
        alg = 'bisection'
    #-------</input checking>-------#
    f_l = fun(x_l, *args, **kwargs)
    f_r = fun(x_r, *args, **kwargs)
    b_l = b(x_l) if flag_b_func else b
    p_l = np.sign(f_l-b_l)
    x_b = x_l
    f_b = fun(x_b, *args, **kwargs)
    b_b = b(x_b) if flag_b_func else b
    get_xEst_reg = lambda x_l, x_r, f_l, f_r, b_l: (b_l-f_l)*(x_r-x_l)/(f_r-f_l) + x_l
    get_xEst_bis = lambda x_l, x_r, *args: .5*(x_l + x_r)
    get_xEst = get_xEst_reg if alg=='regula' else get_xEst_bis
    k = 0
    while (abs(f_b-b_b) > tol_b) and (abs(x_r-x_l) > tol_x) and (k < max_iter):
        x_b = get_xEst(x_l, x_r, f_l, f_r, b_l)
        f_b = fun(x_b, *args, **kwargs)
        b_b = b(x_b) if flag_b_func else b
        if ((f_b-b_b)*p_l) > 0.:
            x_l = x_b
            f_l = fun(x_l, *args, **kwargs)
        else:
            x_r = x_b
            f_r = fun(x_r, *args, **kwargs)
        k += 1
    if k >= max_iter:
        print("Warning: maximum number of iterations reached")
    if print_n:
        print("Number of iterations: {:d}".format(k))
    return x_b

def calc_mu_90(d):
    """
    Given data that are scaled to be uniform on [0,1] by the known signal, calculate
    the 90% C.L. upper limit on the number of possible signal events.
    
    Input d must be a 1-D numpy array
    
    """
    x_r = 10*len(d)
    mu90 = root_search(calc_p_max,0,x_r,p_max_90,d,alg='bisection')
    return mu90





