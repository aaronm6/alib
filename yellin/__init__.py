"""
Library for using Yellin's methods for setting upper limits in the presence of an unknown background.
So far, only p_max is implemented (as this is the only of Yellin's methods that I really
want to use).

This follow's from Yellin's paper:
S. Yellin, "Finding an upper limit in the presence of an unknown background"
Phys. Rev. D66, 032005 (2002)
arXiv:physics/0203002

2014.09.10
"""

from .p_max import *
