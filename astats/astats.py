"""
This module provides some comfort functions for doing stats, particularly random-number
generation, in situations where the builtin scipy libraries may lack certain features.

To get an up-to-date list of available functions, try:

>>> from aLib.misc import getFuns
>>> import astats
>>> getFuns(astats)

Each function therein should have adequate self documentation.

2014.04.26
"""

from scipy import stats as _st
from numpy.random import rand as _rand
from scipy import special as _sp
import numpy as _np

def binornd(n, p, size=[1]):
    """
    Generate an ndarray of random numbers chosen from a binomial distribution.  The user
    can specify a single n, p, or arrays of each.  If n and p are scalars, the array 
    size can be specified with the size input.
    
    ------ Inputs:
           n: Number of trials.  If n is an ndarray, each random number generated 
              separately corresponds to an entry in n.
           p: Probability of success.  If p is an ndarray, each random number generated 
              separately corresponds to an entry in p.  If both n and p are ndarrays, 
              they must be identical dimensions.
        size: If both n and p are scalars, the size of the output ndarray is specified
              by this parameter, which is an iterable (ndarray, list, tuple).  If n 
              and/or p are arrays, size is ignored.
    ----- Outputs:
        bOut: ndarray of binomial random numbers.
    
    Example:
    In [1]: n = round(5*rand(20)+10)
    In [2]: bRands = binornd(n, 0.4)
    
    2014.04.26
    """
    # -- <Input checking> -- #
    if type(n) != _np.ndarray:
        if (type(n) == tuple) or (type(n) == list):
            n = _np.array(n)
        else:
            n = _np.array([n])
    if type(p) != _np.ndarray:
        if (type(p) == tuple) or (type(p) == list):
            p = _np.array(p)
        else:
            p = _np.array([p])
    if (n.size != 1) or (p.size != 1):
        flag_useSize = False
    else:
        flag_useSize = True
    if (n.size != 1) and (p.size != 1):
        if (n.size != p.size) or (n.ndim != p.ndim) or (n.shape[0] != p.shape[0]):
            raise ValueError('If both n and p are arrays, they must be of equal dimensions.')
    if _np.any(_np.round(n) != n):
        raise ValueError('All elements of n must be of integer value (but not necessarily integer type).')
    if any(p<0) or any(p>1):
        raise ValueError('All elements of p must live between zero and one.')
    if flag_useSize:
        if (type(size) != tuple) and (type(size) != list) and (type(size) != ndarray):
            raise ValueError("Input 'size' must be a tuple, list, or ndarray.")
    # -- </Input checking> -- #
    
    # Generate random numbers in the case that n and p are scalars, according to input 'size'
    if flag_useSize:
        bOut = _st.binom.rvs(n, p, size=size)
    else:
        if n.size == 1:
            n = n*_np.ones_like(p)
        if p.size == 1:
            p = p*_np.ones_like(n)
        bOut = _np.zeros_like(n)
        for k in range(1,_np.uint64(n.max()+1)):
            cutN = n >= k
            bOut[cutN] += _rand(cutN.sum()) < p[cutN]
    
    if len(bOut) == 1:
        bOut = bOut[0]
    return bOut
    

def doubleExpPDF(x, L1, L2, a):
    """
    Double exponential distribution PDF.  This is not the "cuspy" Laplace distribution, nor the Gumbel
    distribution, both of which show up in a google search for "double exponential distribution".  This 
    is instead simply the distribution whose PDF is the sum of two exponentials:
    
    doubleExpPDF(x, L1, L2, a) = N*(exp(-L1*x) + a*exp(-L2*x)), 
    
    where N is the normalization constant, given by:
    
    N = L1 * L2 / (a*L1 + L2)
    
    ----------------------Inputs:
        x: Independent variable.
       L1: Exponential constant of the first component.
       L2: Exponential constant of the second component.
        a: Relative weight of the second component relative to the first.
    ---------------------Outputs:
       Output is the value of the PDF evaluated at x, given the three free parameters.
    
    2014.06.26
    """
    N = L1 * L2 / (a*L1 + L2)
    outPDF = N * (_np.exp(-L1*x) + a*_np.exp(-L2*x))
    return outPDF

def doubleExpCDF(x, L1, L2, a):
    """
    Double exponential distribution CDF.  See the definition of this distribution in 'doubleExpPDF'.
    
    2014.06.26
    """
    N = L1 * L2 / (a*L1 + L2)
    outCDF = N * ((1-_np.exp(-L1*x))/L1 + a*(1-_np.exp(-L2*x))/L2)
    return outCDF


from scipy.special import erf
def efficUncert(n, k):
    """
    Calculates proper error bars for the efficiency given by k/n.  This
    assumes that n_c came directly from the same data as what went into n,
    but with some cuts.  Treats the efficiency, ep, as the dependent variable
    in a binomial likelihood: L = ep^k * (1-ep)*(n-k)
    WARNING: no input checking at the moment.
    NOTE: When n = k = 0, it assumes the estimate for k/n is zero, and then
    erUp = 0.683 and erDn = 0.  Technically, it should be that k/n = 0.5, but
    whatever.
    
     inputs:
            n: Histogram of original data before cuts.
            k: Histogram of data after cuts.
    outputs:
         erUp: Vector of upward errorbars (1-sig).
         erDn: Vector of downward errorbars (1-sig).
    
    2010.03.21
    """
    raise ValueError("THIS FUNCTION ISN'T WORKING PROPERLY YET, sorry!!")
    ep = _np.linspace(0,1,1e3)
    Lm = k/n  # Lm = likelihood max
    Lm00 = _np.isnan(Lm)
    Lm[Lm00] = 0
    a1sig = erf(1/_np.sqrt(2)) # ~68.3%
    erUp = _np.zeros_like(n)
    erDn = _np.zeros_like(n)
    for t in range(len(n)):
        if not Lm00[t]:
            Lik = (ep**k[t])*(1-ep)**(n[t]-k[t])
            Lik = Lik/Lik.sum()
            prop = _np.linspace(0,Lik.max()*1.01)
            propArea = _np.zeros_like(prop)
            for t1 in range(len(prop)):
                propArea[t1] = Lik[Lik>=prop[t1]].sum()
            prop1sig = prop[_np.nonzero(propArea>a1sig)[0].max()]
            erUp[t] = ep[_np.nonzero(Lik>=prop1sig)[0].max()]-Lm[t]
            erDn[t] = Lm[t] - ep[_np.nonzero(Lik>=prop1sig)[0].max()]
        else:
            erUp[t] = a1sig
            erDn[t] = 0
    return erUp, erDn

_bino = lambda n, k: _sp.binom(n, k).astype(int)
def multinomial_nonzero(n_bins, n_hits):
    """
    In a situation captured by the multinomial distribution, some number of bins 
    (n_bins) can be filled with some number of hits (n_hits).  The total number 
    of possible possible ways for this to occur is n_bins ^ n_hits (which follows
    directly from the Multinomial Theorem).  However, one may wish to know a 
    slightly different question, which is, how many different ways can n_hits be
    distributed to n_bins, *such that no bin is empty*?  This function calculates
    that number.  It returns an array; the first element of the array represents
    the number of ways to distribute n_hits in 1 bin, the second element indicates 
    the number of ways to distribute n_hits in 2 bins (with no empty bins), the 3rd,
    the number of ways to distribute n_hits in 3 bins (with no empty bins), and so 
    on.  The length of the array is n_bins, so the last element in the output array 
    gives the desired number.  An array is returned because (a) the formula for 
    this number is recursive, and (b) applications for this number often require
    the full array.
    
    2021.05.21
    """
    N_nonzer = _np.zeros(n_bins)
    for k in range(n_bins):
        N_nonzer[k] = ((k+1)**n_hits) - (N_nonzer[:k]*_bino(k+1,_np.r_[:k]+1)).sum()
    return N_nonzer.astype(int)

def prob_nfold_fail(n_bins, n_beans, n_fold):
    """
    An interesting situation is in a scintillation detector, where some number of photons
    is distributed uniformly to some number of photon detectors (i.e. PMTs); uniformity is
    assumed for simplicity.  Typically one requires a certain number of PMTs to detect 1 
    or more photons; i.e. one would say that 3-fold coincidence requirement means that at 
    least 3 PMTs must have detected at least 1 photon.  The question is, given a number of 
    detected photons, a number of PMTs and an n-fold coincidence requirement, what is the 
    probability to pass the coincidence requirement? 
    
    This scenario is generalized to a combinatorics question, related to the birthday 
    paradox and the pigeonhole theorem.  The generalized problem dealt with here is
    specifically:
        A number of beans (n_beans) is randomly (and uniformly) distributed into a number
        of bins (n_bins).  We calculate here the probability to obtain fewer than n_fold
        nonempty bins.
    In other words, we calculate here the probability to FAIL the coincidence requirement.
    This makes sense because the probability may often be very-very close to zero, and 
    hence the probability to pass the coincidence requirement may be very-very close to 
    unity. Floating-point precision does very well with numbers very close to zero, but not
    so well with numbers close to a non-zero number.
    
    2021.05.22
    """
    # N_nonzer[k] gives the number of ways to put n_beans into k+1 bins
    N_nonzer = multinomial_nonzero(n_fold-1, n_beans)
    #N_nonzer = _np.zeros(n_fold - 1)
    #for k in range(n_fold-1):
    #    N_nonzer[k] = ((k+1)**n_beans) - (N_nonzer[:k]*_bino(k+1,_np.r_[:k]+1)).sum()
    # N_fail is the total number of ways to fail the coincidence requirement
    N_fail = (N_nonzer * _bino(n_bins, n_bins - _np.r_[1:n_fold])).sum()
    # N_tot is the total number of ways to distribute n_beans into n_bins
    #N_tot = n_bins ** n_beans
    inv_N_tot = n_bins ** (-n_beans)
    
    # The probability to fail coincidence, P_fail is simply N_fail / N_tot
    #P_fail = N_fail / N_tot
    P_fail = inv_N_tot * N_fail
    return P_fail
















