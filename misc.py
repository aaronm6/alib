"""
A bunch of miscellaneous functions.
To see the functions available, type:
>>> misc.getFuns(misc)

2014.03.04
"""

import matplotlib as _mpl
from matplotlib import pyplot as _plt
import numpy as _np
import os as _os
import sys as _sys
import ctypes as _ctypes

def stairs(x, y, *args, **kwargs):
    """
    For use like the Matlab stairs function.  However, numpy's histogram function returns
    a vector with one fewer elements than the bin edges vector, and thus stairs requires
    input 'y' to have one fewer elements than input 'x'.  Note that numpy's histogram
    actually returns a 2-element tuple, so it is good to de-reference the first element as
    in the example below.
    -------------inputs:
              x: (arg, required) Bin EDGES
              y: (arg, required) Bin contents (one fewer element than x).
             fs: (arg, optional) Format string. Ex: 'r-' for a red, solid line.
             y2: (kwarg, optional) For use when using hatching or filling.  This input
                 will set the bottom boundary of the fill area, and should be an object
                 similar to the input 'y'.
          hatch: (kwarg, optional) Hatch format string.  See documentation for 
                 matplotlib.pyplot.fill for the hatch string. For no hatching, set hatch 
                 equal to None, or leave blank.
      facecolor: (kwarg, optional) Color of the fill.  Choose 'none' (not None) for no
                 fill, or don't set.
     hatchcolor: (kwarg, optional) Color of the hatching. If left blank and "hatch" kwarg
                 is set, hatchcolor is set to the color of the step curve.
    ------------Outputs:
              h: Either a single handle (in the case of no filling or hatching), or a list 
                 of two handles if filling and/or hatching is added.  In the latter case, 
                 the first handle is the step line, while the second handle is to the fill 
                 object that contains the filling and/or hatching.
    
    Example 1:
    In [1]: a = randn(1e3)+5
    In [2]: x_edges = linspace(0,10,101)
    In [3]: n_data = histogram(a, x_edges)[0]
    In [4]: figure()
    Out[5]: <matplotlib.figure.Figure at 0x1230a8810>
    In [6]: stairs(x_edges, n_data, 'b-', linewidth=2)
    
    Example 2 (from the last line of Example 1):
    In [6]: stairs(x_edges, n_data, 'b-', hatch='/////')
    
    2014.03.04
    2014.03.28 - Added functionality for filling the histogram with either 
                 solid color (possibly with non-unity alpha), and/or 
                 hatching.  The previous version of this function used the 
                 built-in matlab.pyplot.step function.  However, this tool 
                 does not support filling/hatching, so a custom job is 
                 utilized here.
    """
    if 'y2' in kwargs.keys():
        y2 = kwargs.pop('y2')
    else:
        y2 = -1
    def createStep(x,y):
        xx = _np.tile(x,[2,1]).T.flatten()[1:-1]
        yy = _np.tile(y,[2,1]).T.flatten()[:-2]
        return xx, yy
        
    #if any([opt in kwargs.keys() for opt in ("hatch","facecolor","hatchcolor")]):
    xx, yy = createStep(x,_np.hstack([y,y[-1]]))
    
    flag_y2 = False
    if type(y2)==_np.ndarray:
        flag_y2 = True
    
    if flag_y2:
        xx, y2 = createStep(x, _np.hstack([y2,y2[-1]]))
    
    if "hatch" in kwargs.keys():
        hatch = kwargs.pop('hatch')
    else:
        hatch = None
    
    if "facecolor" in kwargs.keys():
        facecolor = kwargs.pop('facecolor')
    else:
        facecolor = 'none'
    
    if "hatchcolor" in kwargs.keys():
        hatchcolor = kwargs.pop('hatchcolor')
    else:
        hatchcolor = None
    
    h = []
    tH = _plt.plot(xx, yy, *args, **kwargs)
    if type(tH) == list:
        tH = tH[0]
    h.append(tH)
    if (hatch is not None) or (facecolor != 'none') or (hatchcolor is not None):
        ymin, ymax = _plt.ylim()
        if hatchcolor is None:
            hatchcolor = tH.get_color()
        tH = _plt.fill_between(xx, 
                                     yy,
                                     y2=y2,
                                     hatch=hatch,
                                     edgecolor=hatchcolor,
                                     facecolor=facecolor,
                                     linewidth=0)
        if type(tH) == list:
            tH = tH[0]
        h.append(tH)
        _plt.ylim(ymin,ymax)
    
    #else:
    #    h = _plt.step(x,_np.hstack([y,y[-1]]),*args,where='post',**kwargs)
    
    if len(h)==1:
        h = h[0]
    return h

def lstairs(x, y, *args, **kwargs):
    """
    Like stairs, but it defaults to a logarithmic vertical scale, while allowing
    continuity of the step line when a bin has zero entries.  See the help documentation
    for misc.stairs. May give funky behavior when input y is not counts.
    
    2014.03.12
    2014.03.28 - Along with 'stairs', added support for filling the histogram 
                 with a solid/transparent color and/or a hatching pattern.
    """
    
    if 'y2' in kwargs.keys():
        y2 = kwargs.pop('y2')
    else:
        y2 = 5e-11
    
    n = _np.float64(y.copy())
    n[n<=0] = 1e-50
    
    flag_y2 = False
    if type(y2)==_np.ndarray:
        flag_y2 = True
    
    if flag_y2:
        n2 = _np.float64(y2.copy())
        n2[n2<=0] = 1e-50
    
    def createStep(x,y):
        xx = _np.tile(x,[2,1]).T.flatten()[1:-1]
        yy = _np.tile(y,[2,1]).T.flatten()[:-2]
        return xx, yy
    
    #if any([opt in kwargs.keys() for opt in ("hatch","facecolor","hatchcolor")]):
    xx, nn = createStep(x,_np.hstack([n,n[-1]]))
    
    if flag_y2:
        xx, y2 = createStep(x,_np.hstack([n2,n2[-1]]))
    
    if "hatch" in kwargs.keys():
        hatch = kwargs.pop('hatch')
    else:
        hatch = None
    
    if "facecolor" in kwargs.keys():
        facecolor = kwargs.pop('facecolor')
    else:
        facecolor = 'none'
    
    if "hatchcolor" in kwargs.keys():
        hatchcolor = kwargs.pop('hatchcolor')
    else:
        hatchcolor = None
    
    h = []
    tH = _plt.plot(xx, nn, *args, **kwargs)
    if type(tH) == list:
        tH = tH[0]
    h.append(tH)
    if (hatch is not None) or (facecolor != 'none') or (hatchcolor is not None):
        ymin, ymax = _plt.ylim()
        if hatchcolor is None:
            hatchcolor = tH.get_color()
        tH = _plt.fill_between(xx,
                                     nn,
                                     y2,
                                     hatch=hatch,
                                     edgecolor=hatchcolor,
                                     facecolor=facecolor,
                                     linewidth=0)
        if type(tH) == list:
            tH = tH[0]
        h.append(tH)
        _plt.ylim(ymin,ymax)
    
    #else:
    #    h = _plt.step(x,_np.hstack([y,y[-1]]),*args,where='post',**kwargs)
    
    #_plt.step(x,_np.hstack([n,1e-10]),*args,where='post',**kwargs)
    yMax = _plt.gca().get_ylim()[-1]
    yMax = 10**_np.ceil(_np.log10(yMax))
    _plt.gca().set_yscale('log')
    _plt.ylim([0.8, yMax])
    
    if len(h)==1:
        h = h[0]
    return h

def gcfn():
    """
    Returns the number of the current figure (the one into which shit will get plotted if
    you just blindly executed a "plot" command).  Works similar to the Matlab 'gcf'
    function; the problem with the matplotlib version of gcf is that it returns the figure
    handle, which is not the figure number as it is in Matlab.  Here 'gcfn' stands for
    'get current figure number'.
    
    No inputs.
    
    2014.03.12
    2014.12.11 - Simplified the code; it no longer needs to go through and 
                 update all open figures just to return the number of the current figure.
    """
    return _plt.gcf().number

def fup():
    """
    Try to update the current figure.  No inputs.
    
    2014.12.11
    """
    _plt.figure(gcfn())

def plot2d(x, y, x_range=None, y_range=None, x_bins=50, y_bins=50, flag_log=False, flag_mask=True, cmap=None):
    """
    Plot a 2d histogram in color from some data.  This produces something similar to
    'hist2d', but with some important differences.  First, hist2d produces a pixel image,
    while plot2d produces a vector image.  Second, hist2d (because it is an image) draws a
    rectangle for all bins, while plot2d can plot only non-zero bins (or not, this is
    selectable).  But most importantly (as the first point was made), this plot will be
    *scalable*!
    
             inputs:
             x: 1-D ndarray of x values.
             y: 1-D ndarray of y values
       x_range: (optional) The range in x to make the histogram. Default behavior is to
                make the first edge at the lowest x-value, and the last edge at the
                highest x-value.
       y_range: (optional) Same as x_range, but for the y-values (obviously)
        x_bins: (optional) Number of bins in the x-directions.  Default is 50 bins.
        y_bins: (optional) Number of bins in the y-directions.  Default is 50 bins.
      flag_log: (optional) Boolean (True or False) specifying whether the color scale
                should be linear (False) or logarithmic (True).  Default is False (linear
                scale).
     flag_mask: (optional) Boolean specifying whether or not to throw out bins that have
                no content; True=throw out the empty bins, False=keep them. Default is
                True.
          cmap: (optional) The colormap object to use for the color scale and colorbar.
                Default is the standard blue-green-red "jet" colormap.  Other colormaps
                are constructed in matplotlib.cm.
            Outputs:
      p_handle: Handle to the QuadMesh collection containing all the colorful boxes.
    
    Example:
    In [1]: x = randn(1e4)
    In [2]: y = randn(1e4)
    In [3]: colMap = matplotlib.cm.hot
    In [4]: plot2d(x, y, [-5,5], [-5,5], 70, 70, cmap=colMap)
    In [5]: colorbar()
    In [6]: minorticks_on()
    In [7]: cbarmticks()
    
    2014.03.19
    2014.04.14 - The histogram is now displayed with pcolormesh, rather than the
                 hard-coded PatchCollection.
    2014.04.16 - Added the capability to toggle whether or not to mask out the 
                 empty bins.
    """
    
    #**** <INPUT HANDLING> ****#
    if cmap is None:
        cmap = _mpl.cm.viridis
    if not isinstance(cmap,type(_mpl.cm.jet)) and \
        not isinstance(cmap,type(_mpl.cm.plasma)):
        raise TypeError("input cmap must be a matplotlib color map object.")
    if x_range is None:
        x_range = [x.min(), x.max()]
    if y_range is None:
        y_range = [y.min(), y.max()]
    if (x_range[1]-x_range[0])<0:
        x_range = x_range[::-1]
    if (y_range[1]-y_range[0])<0:
        y_range = y_range[::-1]
    #**** </INPUT HANDLING> ****#
    
    # Make the bin edges
    x_xe = _np.linspace(x_range[0], x_range[1], x_bins+1) # add 1 because these are bin edges
    x_ye = _np.linspace(y_range[0], y_range[1], y_bins+1) # add 1 because these are bin edges
    x_width = _np.abs(x_xe[1]-x_xe[0])
    y_width = _np.abs(x_ye[1]-x_ye[0])
    
    # Calculate the 2-D histogram and flatten it
    n = _np.histogramdd(_np.array([x,y]).T, [x_xe, x_ye])[0]
    
    if flag_mask:
        n_max = n[n>0].max()
        n_min = n[n>0].min()
        n = _np.ma.masked_where(n<=0, n)
    else:
        if flag_log:
            n[n<=0] = 0.1
        n_max = n.max()
        n_min = n.min()
    
    # Create a normalization function based on whether the user wants a lin or log color scale.
    if flag_log:
        n_Norm = _mpl.colors.LogNorm(vmin=n_min, vmax=n_max)
    else:
        n_Norm = _mpl.colors.Normalize(vmin=n_min, vmax=n_max)
    
    h = _plt.pcolormesh(x_xe, x_ye, n.T, norm=n_Norm, cmap=cmap)
    h.set_edgecolor('face')
    h.set_linewidth(0.1)
    if flag_log and (not flag_mask):
        cMax = h.get_clim()[1]
        h.set_clim((.8,cMax))
    fup()
    return h

def plot2d_old(x, y, x_range=None, y_range=None, x_bins=50, y_bins=50, flag_log=False, 
    cmap=None, alpha=1., boxedgewidth=0.1):
    """
    Use plot2d instead; this is the old version and kept here only for legacy purposes,
    and as a useful reference for how to work with Collections classes.
    
    Update(2014.07.19) Ok ok, this is still useful if you want transparent bins. Woohoo.
    """
    
    #**** <INPUT HANDLING> ****#
    if cmap is None:
        cmap = _mpl.cm.viridis
    if not isinstance(cmap,type(_mpl.cm.jet)) and \
        not isinstance(cmap,type(_mpl.cm.plasma)):
        raise TypeError("input cmap must be a matplotlib color map object.")
    if x_range is None:
        x_range = [x.min(), x.max()]
    if y_range is None:
        y_range = [y.min(), y.max()]
    if (x_range[1]-x_range[0])<0:
        x_range = x_range[::-1]
    if (y_range[1]-y_range[0])<0:
        y_range = y_range[::-1]
    #**** </INPUT HANDLING> ****#
    
    # Make the bin edges
    x_xe = _np.linspace(x_range[0], x_range[1], x_bins+1) # add 1 because these are bin edges
    x_ye = _np.linspace(y_range[0], y_range[1], y_bins+1) # add 1 because these are bin edges
    x_width = _np.abs(x_xe[1]-x_xe[0])
    y_width = _np.abs(x_ye[1]-x_ye[0])
    
    # Calculate the 2-D histogram and flatten it
    n = _np.histogramdd(_np.array([x,y]).T, [x_xe, x_ye])[0]
    n_fl = n.flatten()
    
    # Create the list of patches, where each patch is a bin, and cut out the bins with zero entries.
    patches = [_mpl.patches.Rectangle((xval,yval),x_width,y_width,fill=True) for xval in x_xe[:-1] \
        for yval in x_ye[:-1]]
    patches = [patches[k] for k in range(len(patches)) if n_fl[k]!=0]
    n_fl = n_fl[n_fl!=0]
    n_max = n_fl.max()
    n_min = n_fl.min()
    
    # Create a normalization function based on whether the user wants a lin or log color scale.
    if flag_log:
        n_Norm = _mpl.colors.LogNorm(vmin=n_min, vmax=n_max)
    else:
        n_Norm = _mpl.colors.Normalize(vmin=n_min, vmax=n_max)
    
    # Create a PatchCollection from the list of patches and set the histogram as its data
    p = _mpl.collections.PatchCollection(patches, cmap=cmap, norm=n_Norm, alpha=alpha,
                                        edgecolor="face",linewidth=boxedgewidth)
    p.set_array(n_fl)
    
    # Put the PatchCollection into the current axis, set the axis limits.
    _plt.gca().add_collection(p)
    _plt.axis([x_range[0],x_range[1],y_range[0],y_range[1]])
    
    fup()
    return p

def getchildren(ax=None):
    """
    Get the handles to the objects in the current axis, but only ones that have been added
    by the user (not including stuff like axis labels, lines, etc.).
    
        inputs:
           ax: (Optional) The axes in which you would like to search.  If no axes object
               is provided, it will take the one that is returned by
               matplotlib.pyplot.gca().
       Outputs:
           hh: A list of handles to the objects in the axes.
    
    2014.03.18
    """
    if ax == None:
        ax = _plt.gca()
    
    hh = [h for h in ax.get_children() 
        if h._remove_method is not None and not isinstance(h,_mpl.legend.Legend)]
    
    return hh

def deletelastchild(n=1):
    """
    misc.deletelastchild:
    Delete the last child in the current figure axis, which has the highest zorder value.
    No output.
    
    Single argument, n: delete the last n values, not just the last one.  Default
                        is n=1
    
    2014.03.11
    2014.03.18 - Update: Using the function "getchildren" in this module
    2014.04.30 - Matplotlib does not hold a strict correspondence between the 
                 order of objects in ax.get_children() and the order in which 
                 they were added to the current axis. Nor is there a 
                 correspondence with the layering of objects in the plot.  This
                 can be a problem when using fill objects.  I have now modified 
                 it so that it will delete the last object that is on TOP. This
                 is handled by considering only objects whose zorder parameter 
                 is equal to the maximum zorder of all objects in the axis.
    """
    hCh = getchildren()
    hh = [h for h in hCh if h.get_zorder()==max([h0.get_zorder() for h0 in hCh])]
    if len(hh)>0:
        numRemove = min(n, len(hh))
        for k in range(numRemove):
            hh[-(k+1)].remove()
    
    fup()

def mfindobj(obj, **kwargs):
    """
    misc.mfindobj:
    Search an object for sub-objects that match certain properties; usage/philosophy is 
    similar to Matlab's 'findobj' function. Properties to search for must be member 
    functions of the objects contained in the input "obj", and these member functions must 
    take zero arguments (i.e. get_color, or get_markersize, see below).
    
    Example:
    In [1]: misc.mfindobj(gca(), get_color='b', get_markersize=6)
    Out[1]: <matplotlib.lines.Line2D at 0x109b73d90>
    
    2014.03.12
    """
    if hasattr(obj, 'findobj'):
        found_list = []
        hh = obj.findobj(include_self=False)
        for h in hh:
            tFound = True
            for prop in kwargs.keys():
                tFound = tFound & hasattr(h, prop)
                if tFound:
                    tFound = tFound & (eval("h.{:s}()".format(prop))==kwargs[prop])
            if tFound:
                found_list.append(h)
        if len(found_list)==0:
            return "No matching objects"
        elif len(found_list)==1:
            return found_list[0]
        else:
            return found_list
    else:
        print("Object provided has no 'findobj' method")

def eff(cut):
    """
    eff: Returns the efficiency of a 1-D boolean array.
    If input "cut" is not of type numpy.ndarray, this function returns nothing.
    If cut.dtype is not "bool", it returns the fraction of non-zero elements.
    
    2014.03.12
    """
    if isinstance(cut, _np.ndarray):
        if cut.dtype == bool:
            effic = cut.sum().astype(float) / len(cut)
        else:
            effic = (cut!=0).sum().astype(float)/len(cut)
    else:
        effic = None
    return effic

def xsh(x):
    """
    xsh takes an ordered array and shifts the elements by half of their spacing.  This is 
    useful, for example, when you want to turn bin edges into bin centers.  The returned 
    numpy array has one fewer elements than the input array.  input array can be non-
    uniformly spaced, but must be monotonically increasing.
    
    2014.03.13
    """
    return x[:-1]+.5*_np.diff(x)

def inrange(a, *args):
    """
    Returns a boolean ndarray the same length/size of input data 'a', indicating where a
    is in range of values specified.
    
        inputs:
          a: ndarray of data from which to analyze.
       args: Either one or two inputs. If one input is given, it should be a python list 
             object specifying [a_min, a_max].  If two arguments are given, the first
             should be a_min, the second should be a_max.  If a_min is greater than a_max,
             it blindly switches them.
       Outputs:
        cut: Boolean ndarray the same length/size as input a.  Logically, this represents 
             the following statement:
                (a >= a_min) & (a < a_max)
    
    2014.03.18
    """
    # -----<INPUT HANDLING>----- #
    #if type(a) != _np.ndarray:
    #    raise TypeError("input data must be a numpy array (type numpy.ndarray).")
    if len(args) == 1:
        if len(args[0]) != 2:
            raise TypeError("If only one extra argument given, it must be a 2-element list.")
        a_min = args[0][0]
        a_max = args[0][1]
    else:
        a_min = args[0]
        a_max = args[1]
    if a_min > a_max:
        a_min, a_max = a_max, a_min
    # -----</INPUT HANDLING>----- #
    
    cut = (a >= a_min) & (a < a_max)
    return cut

# This code creates a special legend item for 2-D histograms... essentially a small colorbar
from matplotlib.legend_handler import HandlerBase
from matplotlib.collections import QuadMesh
class HandlerQuadMesh(HandlerBase):
    def __init__(self, numPatches=101, **kw):
        self.numPatches = numPatches
        HandlerBase.__init__(self, **kw)
    def create_artists(self, legend, orig_handle, xdescent, ydescent, width, height, fontsize, trans):
        Nx = self.numPatches  # number of x-edges; number of rectangles will be Nx-1
        Ny = 2   # same thing about Nx
        x = _np.linspace(-xdescent,-xdescent+width,Nx)
        y = _np.linspace(-ydescent,-ydescent+height,Ny)
        c = _np.linspace(0,1,Nx-1)
        x = x.reshape(1, Nx)
        x = x.repeat(Ny, axis=0)
        y = y.reshape(Ny, 1)
        y = y.repeat(Nx, axis=1)
        x = x.ravel()
        y = y.ravel()
        coords = _np.c_[x,y]
        col_qd = QuadMesh(Nx-1, Ny-1, coords)
        col_qd.set_array(c)
        orig_cmap = None
        if hasattr(orig_handle, 'get_cmap'):
            orig_cmap = orig_handle.get_cmap()
        if orig_cmap is None:
            orig_cmap = cm.jet
        orig_alpha = None
        if hasattr(orig_handle, 'get_alpha'):
            orig_alpha = orig_handle.get_alpha()
        if orig_alpha is not None:
            col_qd.set_alpha(orig_alpha)
        orig_lw = 2.
        if hasattr(orig_handle, 'get_linewidth'):
            orig_lw = orig_handle.get_linewidth()
        col_qd.set_cmap(orig_handle.get_cmap())
        col_qd.set_edgecolor('face')
        col_qd.set_linewidth(orig_lw)
        return [col_qd]

from matplotlib.legend import Legend
from matplotlib.collections import PatchCollection
Legend.update_default_handler_map({QuadMesh: HandlerQuadMesh(), PatchCollection: HandlerQuadMesh()})

def leg(*args, **kwargs):
    """
    A wrapper for the matplotlib.pyplot.legend function.
    If no arguments are given, it searches the plot for plotted elements and creates a
    legend with all elements, labeled by their index in the list of elements.
    
    To select and/or reorder certain elements for selection in a legend, N+1 arguments
    must be provided, where N is the number of desired entries in the legend.  The first
    argument must be a list of ints indicating which elements to add (and in which order),
    which is taken from the legend produced by leg().  The remaining N arguments are
    strings that indicate the associated labels. A handle to the legend object is
    returned.
    
    A keyword argument 'ax' can be give, which should be an instance of an axes.  If none
    is given, it takes the axes returned by gca() (i.e. matplotlib.pyplot.gca).
    
    Example:
    In [1]: figure()
    In [2]: plot(randn(10,5),'-')
    In [3]: leg() #produce a legend with all elements and their indices
    In [4]: leg([4,2],'Data','Monte Carlo') # pick only the 5th and 3rd elements (in that
            # order) and label.
    
    2014.03.20
    """
    #------<INPUT HANDLING>------#
    if len(args)>0:
        if not isinstance(args[0],list):
            raise TypeError("First provided argument must be a list of ints.")
        if [isinstance(aa,int) for aa in args[0]].count(True) != len(args[0]):
            raise TypeError("First provided argument must be a list of ints.")
        if len(args[0]) != len(args[1:]):
            raise TypeError("The length of the first provided argument must be the same as\n" + \
                            "\t   the number of remaining arguments")
        if [isinstance(aa,str) for aa in args[1:]].count(True) != len(args[0]):
            raise TypeError("All arguments after the first one must be strings.")
    #------</INPUT HANDLING>------#
    ax = _plt.gca()
    if 'ax' in kwargs:
        ax = kwargs.pop('ax')
    hh = getchildren(ax=ax) # function above from this module
    # construct handlermap dictionary
    hMap = {}
    for h in hh:
        #if hasattr(h,'_linestyle'):
        if isinstance(h, _mpl.lines.Line2D):
            hMap[h] = _mpl.legend_handler.HandlerLine2D(numpoints=1)
    if len(args)==0:
        #h_L = _plt.legend(hh,["{:d}".format(k) for k in range(len(hh))],
        #      handler_map=hMap,**kwargs)
        h_L = ax.legend(hh,["{:d}".format(k) for k in range(len(hh))],
              handler_map=hMap,**kwargs)
    else:
        #h_L = _plt.legend([hh[k] for k in args[0]],args[1:],
        #      handler_map=hMap,**kwargs)
        h_L = ax.legend([hh[k] for k in args[0]],args[1:],
              handler_map=hMap,**kwargs)
    return h_L

def legloc(legPos, ax=None, legHandle=None):
    """
    Set the location of the legend.  A relative location can be given as a string, or
    actual coordinates can be given.
    
    -----------inputs:
         legPos: Position input.  This input can be a string, indicating the relative
                 location, in which case it must be one of the following strings:
                    'best'
                    'upper right'
                    'upper left'
                    'lower left'
                    'lower right'
                    'right'
                    'center left'
                    'center right'
                    'lower center'
                    'upper center'
                    'center'
                 Alternatively, a length-2 tuple or list can be given that specifies the
                 x-y coordinates of the lower-left corner of the legend.  The coordinates
                 are in the range [0,1], and relate to the position in the figure (not the
                 axes).
             ax: (Optional) The handle to the axes in which to adjust the legend. Default:
                 gca()
      legHandle: (Optional) The handle to the legend to be adjusted.  Default: the legend
                 found in the current (or specified) axes.
    ----------Outputs:
      legHandle: The handle to the legend that was adjusted.
      
    Example:
    In [1]: figure()
    In [2]: plot(randn(10,5),'-')
    In [3]: leg() # "leg" from this module
    In [4]: leg([4,2],'Data','Monte Carlo') 
    In [5]: legloc('lower left')
    
    2014.03.28
    """
    locDict = { 
        'best':0,
        'upper right':1,
        'upper left':2,
        'lower left':3,
        'lower right':4,
        'right':5,
        'center left':6,
        'center right':7,
        'lower center':8,
        'upper center':9,
        'center':10}
    if type(legPos) == str:
        if legPos not in locDict.keys():
            raise ValueError("Your location string must match one listed in the documentation.")
        legPosNum = locDict[legPos]
    elif (type(legPos)==tuple) or (type(legPos)==list):
        if len(legPos)!=2:
            raise ValueError("Your location list/tuple must be 2 elements.")
        legPosNum = legPos
    else:
        raise ValueError("I don't recognize your legend location input.")
    
    if ax is None:
        ax = _plt.gca()
    if legHandle is None:
        legHandle = ax.get_legend()
    
    legHandle._set_loc(legPosNum)
    fup()
    return legHandle

import types
def getFuns(modName):
    """
    Return a list of the names of functions contained in a module.
    --------inputs:
      modName: The module that is to be searched.
    -------Outputs:
      funList: A list of names of the functions contained in modName.
      
    2014.03.24
    """
    funList = [modName.__dict__.get(a).__name__ for a in dir(modName) \
        if isinstance(modName.__dict__.get(a), types.FunctionType)]
    return funList

class getObj:
    """
    For an object of a class whose __repr__ method has been customized, and the class name
    and memory location are desired, this function can get them.  The returned string is
    identical to what would be output if the class (and its super classes) don't have a
    __repr__ method specified.  This is useful to see if a frequently modified object is
    getting overwritten, or re-instantiated, for example.
    
    Example:
    In [1]: x = 3
    In [2]: x
    Out[2]: 3
    In [3]: getObj(x)
    Out[3]: <builtins.int object at 0x1076642b0>
    
    """
    def __init__(self, obj):
        self.objStr = "<{:s}.{:s} object at {:s}>".format(obj.__class__.__module__,
                                                     obj.__class__.__name__,
                                                     hex(id(obj)))
    def __str__(self):
        return self.objStr
    def __repr__(self):
        return self.objStr

def cbarmticks(cbar=None, ax=None, side='both'):
    """
    Turn on minorticks in a colorbar, and puts ticks on both the left and right.  Works
    for both linear and logarithmic color scales.  If the clim is reset after applying
    minor ticks, and the color scale is logarithmic, this function will need to be run
    again.
    ---------------------- inputs: -----------------------:
        cbar: (Optional) kwarg specifying the handle of the colorbar to modify.  Default:
              the first colorbar found in ax.
          ax: (Optional) kwarg specifying the handle of the axes in which to make these
              changes. Default: the axes handle returned by gca()
        side: (Optional) The side of the colorbar where tick marks should be located.  
              Matplotlib creates them on the right only, by default, but the default for
              this function is to put them on both side.  Possible values of this string
              kwarg are 'left','right','both','default','none'. Default: 'both'.
    ----------------------Outputs: -----------------------:
        cbar: The handle to the colorbar that was modified.
    
    2014.04.10
    2014.05.25 -- Added the optional default feature to add tick marks to both 
                  sides of the colorbar (not just the right side as is done by 
                  default by matplotlib).
    """
    if ax is None:
        ax = _plt.gca()
    if cbar is None:
        objWcbarNoNone = [hh for hh in getchildren(ax) if hasattr(hh,'colorbar') and (hh.colorbar is not None)]
        if len(objWcbarNoNone) != 0:
            cbar = objWcbarNoNone[0].colorbar
    if cbar is None:
        raise ValueError('No colorbar found')
    
    if isinstance(cbar.norm, _mpl.colors.LogNorm):
        cbar.ax.yaxis.set_ticks_position('both')
        '''
        vmin = cbar.vmin
        vmax = cbar.vmax
        locs = cbar.ax.yaxis.get_major_locator().locs
        locsExpts = locs * (_np.log10(vmax) - _np.log10(vmin)) + _np.log10(vmin)
        locsExpts = _np.hstack([locsExpts[0]-1,locsExpts,locsExpts[-1]+1])
        locsN = 10**locsExpts
        
        minLocsN = _np.tile(locsN,[9,1]).T.cumsum(1)[:,1:].flatten()
        minLocsN = minLocsN[(minLocsN>=vmin)&(minLocsN<=vmax)]
        minlocs = (_np.log10(minLocsN)-_np.log10(vmin))/(_np.log10(vmax) - _np.log10(vmin))
        cbar.ax.yaxis.set_minor_locator(_mpl.ticker.FixedLocator(minlocs))
        '''
    else:
        cbar.ax.minorticks_on()
    
    # This line makes the colorbar look nice when viewed in OS X Preview, for Apple users.
    cbar.solids.set_edgecolor('face')
    
    # Set the side where tick marks will be located. Changes them to 'both' by default, but is 
    #controlled by kwarg 'side'.
    cbar.ax.yaxis.set_ticks_position(side)
    
    fup()
    return cbar

def cbarylabel(string, ax=None, cbar=None, **kwargs):
    """
    Set the ylabel of the colorbar, if one exists.  The default behavior of matplotlib is
    to set the rotation of the ylabel such that it reads bottom to top.  Since I don't
    like this, this function hardwires it to be the other way (which requires also a
    change to the 'verticalalignment' kwarg.  Additional kwargs can be added that specify
    the fontsize, etc.
    
    2014.05.09
    """
    if ax == None:
        ax = _plt.gca()
    if cbar == None:
        objWcbarNoNone = [hh for hh in getchildren(ax) if hasattr(hh,'colorbar') and (hh.colorbar!=None)]
        if len(objWcbarNoNone) != 0:
            cbar = objWcbarNoNone[0].colorbar
    if cbar == None:
        raise ValueError('No colorbar found')
    
    cbar.ax.set_ylabel(string, rotation=270., verticalalignment='bottom', **kwargs)
    
    # This line makes the colorbar look nice when viewed in OS X Preview, for Apple users.
    cbar.solids.set_edgecolor('face')
    
    fup()
    return cbar

def ticksdbl(ax=None, which='x'):
    """
    Set the spacing of the major ticks in an axes to be half what they currently are.
    
    inputs:
           ax: (Optional) Handle to the axes which are to be modified.  If left blank, the
               axes returned by gca() are used.
        which: (Optional) A one-character string specifying either 'x' or 'y'.  Default is
               'x'.
    Outputs: (none)
    
    2014.05.03
    """
    if ax==None:
        ax = _plt.gca()
    
    if which=='x':
        axHandle = ax.xaxis
    elif which=='y':
        axHandle = ax.yaxis
    else:
        raise ValueError("input 'which' must be either 'x' or 'y'.")
    
    tickSpacing = _np.diff(axHandle.get_ticklocs()[:2])[0]
    axHandle.set_major_locator(_mpl.ticker.MultipleLocator(.5*tickSpacing))
    fup()

def ticksauto(ax=None, which='b'):
    """
    Set the spacing of the major ticks in an axes to be auto.
    
    inputs:
           ax: (Optional) Handle to the axes which are to be modified.  If left blank, the
               axes returned by gca() are used.
        which: A one-character string specifying either 'x', 'y', or 'b' (for both). 
               Default is 'b'.
    Outputs: (none)
    
    2014.05.05
    """
    if ax==None:
        ax = _plt.gca()
    
    if which=='x':
        axHandle = [ax.xaxis]
    elif which=='y':
        axHandle = [ax.yaxis]
    elif which=='b':
        axHandle = [ax.xaxis, ax.yaxis]
    else:
        raise ValueError("input 'which' must be either 'x' or 'y'.")
    for hax in axHandle:
        hax.set_major_locator(_mpl.ticker.AutoLocator())
    fup()

def tickxm(mult, ax=None):
    """
    Set the multiple to be used as the major tick mark on the x-axis.
    
    inputs:
         mult: Within the range of the horizontal axis, a tick mark will be placed at
               every multiple of the number given as 'mult'.  E.g. if mult=2 and the range
               is [-5,7], then tick marks will be placed at -4, -2, 0, 2, 4, 6.  Mult need
               not be an integer.
           ax: (Optional) Handle to the axes which are to be modified.  If left blank, the
               axes returned by gca() are used.
    Outputs: (none)
    
    2014.09.30
    """
    if ax is None:
        ax = _plt.gca()
    ax.xaxis.set_major_locator(_mpl.ticker.MultipleLocator(mult))
    figNumber = ax.get_figure().number
    _plt.figure(figNumber)

def tickym(mult, ax=None):
    """
    Set the multiple to be used as the major tick mark on the y-axis.
    
    inputs:
         mult: Within the range of the vertical axis, a tick mark will be placed at every 
               multiple of the number given as 'mult'.  E.g. if mult=2 and the range is
               [-5,7], then tick marks will be placed at -4, -2, 0, 2, 4, 6.  Mult need
               not be an integer.
           ax: (Optional) Handle to the axes which are to be modified.  If left blank, the
               axes returned by gca() are used.
    Outputs: (none)
    
    2014.09.30
    """
    if ax is None:
        ax = _plt.gca()
    ax.yaxis.set_major_locator(_mpl.ticker.MultipleLocator(mult))
    figNumber = ax.get_figure().number
    _plt.figure(figNumber)

def figlike(fnumSource, fnumTarget=None):
    """
    Create a figure with the same dimensions as another figure.
    inputs:
        fnumSource: The number of the figure which to copy, or the handle of 
                    the figure object.
        fnumTarget: Optional. If none is supplied, it uses the default
                    next-in-the-sequence like the 'figure' function normally does.
    Outputs:
           fHandle: The handle to the created figure.
    
    2014.12.12
    """
    if isinstance(fnumSource, _mpl.figure.Figure):
        hSource = fnumSource
    else:
        hSource = _plt.figure(fnumSource)
    fHeight = hSource.get_figheight()
    fWidth = hSource.get_figwidth()
    
    fHandle = _plt.figure(fnumTarget,figsize=[fWidth,fHeight])
    return fHandle

from __main__ import __dict__ as mainGlobals
def getSameVars(obj, us=True):
    """
    Find other variable names in the user workspace that point to the same object in
    memory as the one given.  Returns a list of strings, each one a name of a variable
    that points to the address in memory where input 'obj' is located.
    inputs:
          obj: Object to look for.
           us: (kw, Optional): Short for "underscore", this is a boolean kwarg; when true,
               the output list includes items whose variable name starts with an
               underscore ('_'). If false, these underscore-prefixed variables names are
               filtered out. Default: True
    Outputs:
       vNames: List of strings representing the variable names that point to the same mem 
               address as in put obj.
    
    2014.12.28
    """
    vNames = []
    glKeys = mainGlobals.keys()
    for n in glKeys:
        if id(mainGlobals[n])==id(obj):
            vNames.append(n)
    assert type(us)==bool, "input 'us' must be a boolean."
    if not us:
        vNames = [v for v in vNames if v[0]!='_']
    return vNames

def dirg(obj, grepstring, loc="none", invsearch=False, oless=False):
    """
    Similar to the built-in 'dir' function in Python, but it allows the user to filter the
    output based on a search string (similar to the way that 'grep' works at the bash
    command line).
    Inputs:
               obj: Object whose filtered dir output is desired.
        grepstring: String with which to filter output based on
               loc: (optional) Search the whole string if loc='none'; search for strings
                    that start with the search string if loc='start'; search for strings
                    that end with the search string if loc='end'.
         invsearch: (optional) Default = False.  If True, then the search is operated as
                    an exclusion, i.e. it only looks for strings that do not match the
                    search criterion.
             oless: (optional) Default = False.  If True, the output is piped to BASH's
                    'less' command, and the function returns nothing.  If False, it
                    simply returns the list.
    Outputs:
            dirOut: List of strings that meet the search criterion.
    
    2018.03.23
    """
    #   ---------- input checking ----------   #
    assert isinstance(grepstring, str), "input 'grepstring' must be a string."
    assert loc in ("none","start","end"), "input 'loc' must be one of 'none', 'start', 'end'."
    assert isinstance(invsearch, bool), "input 'invsearch' must be True or False."
    #   ---------- /input checking ----------   #
    from operator import xor
    if loc == "none":
        dirOut = [aa for aa in dir(obj) if xor(invsearch, aa.find(grepstring) != -1)]
    elif loc == "start":
        dirOut = [aa for aa in dir(obj) if xor(invsearch, aa.startswith(grepstring))]
    elif loc == "end":
        dirOut = [aa for aa in dir(obj) if xor(invsearch, aa.endswith(grepstring))]
    else:
        dirOut = []
    if oless:
        pless(dirOut.__repr__())
    else:
        return dirOut

from pydoc import pipepager
def pless(text,pretpr = True, pretprThresh=5):
    """
    Pipes the input text to the bash function "less".  But must be run as a function.
    kwarg 'pretpr' controls whether long lists (or other objects with items) will be 
    printed in one, long, wrapped-around string (False), or will be broken up into a
    vertical list like pprint (True).
        Default: True
    kwarg 'pretprThresh' controls the threshold for the number of items input 'text' must 
    have (if it is not a str) in order to trigger the "less"-like behavior.  Default: 5
    
    2018.05.14
    """
    if pretpr and hasattr(text,'__len__'):
        if len(text) > pretprThresh:
            textRepr = text.__repr__()
            newText = ',\n'.join(textRepr.split(','))
            pipepager(newText,'less')
    else:
        pipepager(text.__str__(),'less')

from pprint import pformat
def ppipe(obj, pipe_function='less', ppformat=True):
    """
    Basically just a wrapper function for pipepager, which allows you to pass strings to
    bash string reader functions, like less, cat, head, tail.  Second input specifies
    which bash application to use, default is less.
    
    Input 'obj' is passed to pprint.pformat, which makes it looks nice.  This behavior can
    be toggled.
    
    2018.05.17
    """
    if ppformat:
        thestring = pformat(obj)
    else:
        thestring = obj.__str__()

import time
class tictoc:
    """
    Class for timing bits of code.  The intended use is in a python 'with' block.  
    If a variable is given with 'as', then the timing information is stored in that
    variable.  The output can be suppressed by giving False or 0 in the parentheses.
    
    The following example illustrates its usage for timing how long it takes numpy 
    to sort an ndarray of size 1e5.  
    
    >>> from numpy.random import randn
    >>> a = randn(int(1e5)) # create a random numpy array of 1e5
    >>> with tictoc() as tt:
    ...     a.sort()
    Elapsed time: 1.1538e-2 seconds
    >>> # The elapsed time is stored in variable tt
    >>> tt.t_elapsed
    0.011538028717041016
    >>> # It also works well with string formatting:
    >>> print("Sorting took {:0.4f} seconds".format(tt))
    Sorting took 0.0115 seconds
    >>> # and logical comparisons work
    >>> tt < 0.02
    True
    >>> # logical comparisons with other tictoc objects also work.
    >>> # To suppress the output at run time:
    >>> with tictoc(0) as tt:
    ...    a.sort()
    >>> # casting works as well 
    >>> float(tt)
    0.011538028717041016
    >>> str(tt)
    '0.011538028717041016'
    
    2018.07.02
    """
    def __init__(self, print_on_exit=True):
        self._poe = print_on_exit
        self.t_elapsed = None
    def __enter__(self):
        self.t_start = time.perf_counter()
        return self
    def __exit__(self, thetype, thevalue, traceback):
        self.t_end = time.perf_counter()
        self.t_elapsed = self.t_end - self.t_start
        if self._poe:
            print("Elapsed time: {:1.3e} seconds".format(self.t_elapsed))
    def __repr__(self):
        if self.t_elapsed is not None:
            retString = "{:f} seconds elapsed".format(self.t_elapsed)
        else:
            retString = "tictoc object not used yet"
        return retString
    def __str__(self):
        return str(self.t_elapsed)
    def __float__(self):
        return self.t_elapsed
    def __format__(self, fmtsp):
        allowed_floats = ('e','E','f','F','g','G')
        if fmtsp[-1] in allowed_floats:
            return format(self.__float__(), fmtsp)
        elif fmtsp[-1] == 's':
            return format(self.__str__(), fmtsp)
        else:
            raise ValueError("Unknown format code '{:s}' for object of type 'tictoc'".format(fmtsp))
    def __eq__(self, other):
        if hasattr(other, 't_elapsed'):
            return self.t_elapsed == other.t_elapsed
        elif isinstance(other, float) or isinstance(other, int):
            return self.t_elapsed == other
        else:
            raise TypeError("Incompatible types for comparison")
    def __lt__(self, other):
        if hasattr(other, 't_elapsed'):
            return self.t_elapsed < other.t_elapsed
        elif isinstance(other, float) or isinstance(other, int):
            return self.t_elapsed < other
        else:
            raise TypeError("Incompatible types for comparison")
    def __gt__(self, other):
        if hasattr(other, 't_elapsed'):
            return self.t_elapsed > other.t_elapsed
        elif isinstance(other, float) or isinstance(other, int):
            return self.t_elapsed > other
        else:
            raise TypeError("Incompatible types for comparison")
    def __le__(self, other):
        if hasattr(other, 't_elapsed'):
            return self.t_elapsed <= other.t_elapsed
        elif isinstance(other, float) or isinstance(other, int):
            return self.t_elapsed <= other
        else:
            raise TypeError("Incompatible types for comparison")
    def __ge__(self, other):
        if hasattr(other, 't_elapsed'):
            return self.t_elapsed >= other.t_elapsed
        elif isinstance(other, float) or isinstance(other, int):
            return self.t_elapsed >= other
        else:
            raise TypeError("Incompatible types for comparison")
    def __add__(self, other):
        if hasattr(other, 't_elapsed'):
            outobj = tictoc()
            outobj.t_elapsed = self.t_elapsed + other.t_elapsed
            return outobj
        elif isinstance(other, float) or isinstance(other, int):
            return self.t_elapsed + other
        else:
            raise TypeError("Incompatible types for comparison")
    def __sub__(self, other):
        if hasattr(other, 't_elapsed'):
            outobj = tictoc()
            outobj.t_elapsed = self.t_elapsed - other.t_elapsed
            return outobj
        elif isinstance(other, float) or isinstance(other, int):
            return self.t_elapsed - other
        else:
            raise TypeError("Incompatible types for comparison")
    def __mul__(self, other):
        if hasattr(other, 't_elapsed'):
            outobj = tictoc()
            outobj.t_elapsed = self.t_elapsed * other.t_elapsed
            return outobj
        elif isinstance(other, float) or isinstance(other, int):
            return self.t_elapsed * other
        else:
            raise TypeError("Incompatible types for comparison")
    def __truediv__(self, other):
        if hasattr(other, 't_elapsed'):
            outobj = tictoc()
            outobj.t_elapsed = self.t_elapsed / other.t_elapsed
            return outobj
        elif isinstance(other, float) or isinstance(other, int):
            return self.t_elapsed / other
        else:
            raise TypeError("Incompatible types for comparison")

def cyclerstep(n, ax=None):
    """
    Step matplotlib's property cycler either forward or backward.  Negative numbers 
    are backwards, positive numbers are forward.  If a negative number is given, its
    absolute value cannot be larger than the length of the cycler.
    
    Output: lastProp: a dictionary with the properties of the last item in the cycler
            (i.e. the one that comes before the next one that will be used).
    
    Example: You want to plot two data series, same color but different markers:
    >>> plot(rand(10),'.')
    >>> cycler_step(-1)
    >>> plot(rand(10),'o')
    
    2023.12.20 - updated as "ax._get_lines.prop_cycler" disappeared in mpl v3.8.0
    2018.10.11
    """
    # ---- <input handling> ---- #
    if not isinstance(n, int):
        try:
            n = int(n)
        except:
            raise ValueError("Input given for n cannot be converted to an int")
    if ax is not None:
        assert isinstance(ax,_mpl.axes._subplots.Axes), "Input 'ax' must be an instance \
            of a matplotlib Axes"
    # ---- </input handling> ---- #
    
    if ax is None:
        ax = _plt.gca()
    if hasattr(ax._get_lines, 'prop_cycler'):
        def get_next():
            return next(ax._get_lines.prop_cycler)
    elif hasattr(ax._get_lines,'get_next_color'):
        get_next = ax._get_lines.get_next_color
    else:
        raise RuntimeError("cyclerstep cannot be used with this version of matplotlib")
    #cyclerIterator = ax._get_lines.prop_cycler
    # Get the length of the cycler
    #first = next(cyclerIterator)
    first = get_next()
    lastitem = first
    item = get_next()
    k = 0
    while (item != first) and (k < 1000):
        lastitem = item
        k += 1
        item = get_next()
    if k >= 99:
        raise RuntimeError("cycler is not cycling: more than 1000 items seem to be in it.")
    cyclerLength = k + 1
    item = get_next()
    while item != lastitem:
        item = get_next()
    nCycles = cyclerLength + (n % cyclerLength)
    for k in range(nCycles):
        lastProp = get_next()
    return lastProp

def figsave(f_name, f_handle=None, overwrite=False):
    """
    Save a pdf and png copy of a particular figure.
    Inputs:
        f_name: str, name of file, WITHOUT any file-type suffix (like '.png').
      f_handle: Default: None. Handle to the figure that is to be saved.  If "None", the
                current figure (returned by gcf()) is used.
     overwrite: Default: False (boolean).  If false, it will break if EITHER a pdf or png 
                file in the same directory, with the same name already exists. If True,
                it will overwrite said files without prompt or notification.
    Outputs:
            none

    """
    # ---- <input handling> ---- #
    assert isinstance(f_name, str), "arg 'f_name' must be a string."
    if f_handle is None:
        f_handle = _plt.gcf()
    assert isinstance(f_handle, _mpl.figure.Figure), "kwarg 'f_handle' must be of type matplotlib.figure.Figure."
    # ---- </input handling> ---- #
    if not overwrite:
        pathPrefix = '/'.join(f_name.split('/')[:-1])
        if not pathPrefix:
            pathPrefix = '.'
        dirContent = _os.listdir(pathPrefix)
        fNameOnly = f_name.split('/')[-1]
        if '{:s}.pdf'.format(fNameOnly) in dirContent:
            raise NameError("{:s}.pdf already exists".format(f_name))
        if '{:s}.png'.format(fNameOnly) in dirContent:
            raise NameError("{:s}.png already exists".format(f_name))
    f_handle.savefig("{:s}.pdf".format(f_name), transparent=True)
    f_handle.savefig("{:s}.png".format(f_name))

def gradient_fill(x, y, fill_color=None, ax=None, **kwargs):
    """
    This function copied from:
    https://stackoverflow.com/questions/29321835/is-it-possible-to-get-color-gradients-under-curve-in-matplotlib
    
    NOTE: rcParams["image.composite_image"] = False must be set when making a vector graphic of this.
    
    Plot a line with a linear alpha gradient filled beneath it.
    
    Parameters
    ----------
    x, y : array-like
        The data values of the line.
    fill_color : a matplotlib color specifier (string, tuple) or None
        The color for the fill. If None, the color of the line will be used.
    ax : a matplotlib Axes instance
        The axes to plot on. If None, the current pyplot axes will be used.
    Additional arguments are passed on to matplotlib's ``plot`` function.
    
    Returns
    -------
    line : a Line2D instance
        The line plotted.
    im : an AxesImage instance
        The transparent gradient clipped to just the area beneath the curve.
    """
    if ax is None:
        ax = _plt.gca()
    
    line, = ax.plot(x, y, **kwargs)
    if fill_color is None:
        fill_color = line.get_color()
    
    zorder = line.get_zorder()
    alpha = line.get_alpha()
    alpha = 1.0 if alpha is None else alpha
    
    z = _np.empty((100, 1, 4), dtype=float)
    rgb = _mpl.colors.colorConverter.to_rgb(fill_color)
    z[:,:,:3] = rgb
    z[:,:,-1] = _np.linspace(0, alpha, 100)[:,None]
    
    xmin, xmax, ymin, ymax = x.min(), x.max(), y.min(), y.max()
    im = ax.imshow(z, aspect='auto', extent=[xmin, xmax, ymin, ymax],
                   origin='lower', zorder=zorder)
    
    xy = _np.column_stack([x, y])
    xy = _np.vstack([[xmin, ymin], xy, [xmax, ymin], [xmin, ymin]])
    clip_path = _mpl.patches.Polygon(xy, facecolor='none', edgecolor='none', cl_osed=True)
    ax.add_patch(clip_path)
    im.set_clip_path(clip_path)
    
    ax.autoscale(True)
    return line, im

def hexdump(obj, byteObj=False):
    """
    Displays a representation of the byte content of an object, similar to:
       'hexdump -C <filename> | less'
    on the command line.
    
    If the input 'obj' is a byte string, then that byte string is displayed in this way
    (not the byte content of that object in memory), UNLESS kwarg 'byteObj' is True.
    
    2020.10.20
    """
    if not isinstance(obj, bytes) or byteObj:
        obj = _ctypes.string_at(id(obj), _sys.getsizeof(obj))
    objLen = len(obj)
    bytesPerRow = 16
    if (bytesPerRow % 2) != 0:
        raise ValueError("'bytesPerRow' value must be an even number")
    halfBPR = bytesPerRow // 2
    # Split the byte string into rows of no more than 16 bytes:
    objList = [obj[k:k+(min(bytesPerRow,len(obj)-k))] for k in range(0,objLen,bytesPerRow)]
    numRows = len(objList)
    
    # Now build the output string, row-by-row
    _ostr = '------------------------------------------------------------------------------\n'
    _ostr += 'xaddress  (               hex byte content               )  |  ascii interp  |\n'
    _ostr += '------------------------------------------------------------------------------\n'
    for k, item in enumerate(objList):
        _ostr += '{:08x}  '.format(16*k)
        lenRow = len(item)
        _ostr += ' '.join(['{:02x}'.format(bb) for bb in item[:min(halfBPR,lenRow)]])
        # Pad with spacing if the length was less than 8
        if lenRow <= halfBPR:
            _ostr += ' ' + '   '*(halfBPR-lenRow)
            _ostr += " " # two-space gap between 8-byte series
            _ostr += "   "*halfBPR + " "
        else:
            _ostr += '  '
            _ostr += ' '.join(['{:02x}'.format(bb) for bb in item[halfBPR:min(bytesPerRow,lenRow)]])
            if lenRow < bytesPerRow:
                _ostr += " " + "   "*(bytesPerRow-lenRow)
                _ostr += " "
            else:
                _ostr += '  '
        # ascii representation:
        _ostr += "|"
        _ostr += ''.join([chr(kk) if len(chr(kk).encode('unicode_escape'))==1 else '.' for kk in item])
        _ostr += ' '*(bytesPerRow - lenRow)
        _ostr += '|\n'
    if numRows <= 4:
        print(_ostr.rstrip())
    else:
        pipepager(_ostr.rstrip(), 'less')

from subprocess import run as _run
def cptext(line):
    if not isinstance(line, str):
        raise TypeError("input 'line' must be of type str")
    _run('pbcopy', universal_newlines=True, input=line)


from inspect import signature
def sig(func):
    """
    This function takes another function as an input and print its call signature.
    
    2022.09.06
    """
    t = signature(func)
    print(t)

def squeeze_subplots(
    axis='both',
    fig=None,
    axlims=None,
    x_ep=.002,
    y_ep=None,
    kill_last_xticklabel=True,
    kill_last_yticklabel=True):
    """
    In a figure with a grid of multiple subplots, this function will squeeze the axes
    together so that they are either touching (x_ep and/or y_ep set to zero), or nearly
    touching with a small gap between.
    Inputs:
        axis:    'x': squeeze axes along rows only.
                 'y': squeeze axes along columns only.
              'both': self explanatory
         fig: Figure object in which to perform the action.  If none give, it takes the
              current figure as returned by pyplot.gcf()
      axlims: A length 4 object with x and y axes limits, if desired.  Same inputs as
              in pyplot.axis.  If not specified, it does not adjust axes limits.
        x_ep: The size of the gap between neighboring axes along a row, in units of 
              figure width (a number between 0 and 1).
        y_ep: The size of the gap between neighboring axes along a column, in units of
              figure height (a number between 0 and 1).  If none given, x_ep is applied.
     kill_last_xticklabel: The last tick label of one plot can overlap with the first 
              tick label of the next plot.  If this kwarg is True (the default), then the
              last xtick label will be hidden.
     kill_last_yticklabel: Same as the above, but for the ytick labels.
    """
    if axis not in ('x','y','both'):
        raise ValueError("'axis' must be 'x', 'y', or 'both' (default)")
    if fig is None:
        fig = _plt.gcf()
    if not isinstance(fig, _plt.Figure):
        raise TypeError("fig must be a valid matplotlib axes object, or None for the current one.")
    if axlims is not None:
        if not hasattr(axlims,'__len__'):
            raise AttributeError("'axlims' must be a list-like object (tuple, array, etc.)")
        if len(axlims) != 4:
            raise ValueError("axlims must have a length of 4")
    if y_ep is None:
        y_ep = x_ep
    axs = fig.get_axes()
    nrows, ncols = axs[0].get_subplotspec().get_geometry()[:2]
    k = 0
    while not axs[k].get_subplotspec().is_first_col():
        k += 1
    # get the left x-position
    xp_L = axs[k].get_position().xmin
    xp_orig_width = axs[k].get_position().xmax - xp_L
    
    k = 0
    while not axs[k].get_subplotspec().is_last_col():
        k += 1
    # get the right x-position
    xp_R = axs[k].get_position().xmax
    
    k = 0
    while not axs[k].get_subplotspec().is_first_row():
        k += 1
    # get top y position
    yp_T = axs[k].get_position().ymax
    
    k = 0
    while not axs[k].get_subplotspec().is_last_row():
        k += 1
    # get bottom y position
    yp_B = axs[k].get_position().ymin
    yp_orig_height = axs[k].get_position().ymax - yp_B
    
    if axis in ('x','both'):
        x_width  = (xp_R - xp_L - (ncols-1)*x_ep*2) / ncols
    else:
        x_width = xp_orig_width
    if axis in ('y','both'):
        y_height = (yp_T - yp_B - (nrows-1)*y_ep*2) / nrows
    else:
        y_height = yp_orig_height
    
    for ax in axs:
        ax_geom = ax.get_subplotspec().get_geometry()
        if ax_geom[2] != ax_geom[3]:
            raise ValueError("Functionality for multi-column or multi-row axes not yet implemented.")
        axNum = ax_geom[2]
        colNum = axNum % ncols # left column is col 0, increasing to the right
        rowNum = nrows - 1 - int(axNum/ncols) # bottom row is row 0, increasing upwards
        if axis in ('x','both'):
            pLeft = xp_L + colNum*x_width + colNum*2*x_ep
        else:
            pLeft = ax.get_position().xmin
        if axis in ('y','both'):
            pBot  = yp_B + rowNum*y_height + rowNum*2*y_ep
        else:
            pBot = ax.get_position().ymin
        ax.set_position([pLeft, pBot, x_width, y_height])
    fig.canvas.draw() #needed or else the ticklabels get stuck in a funky state
    for ax in axs:
        axNum = ax.get_subplotspec().get_geometry()[2]
        if (not ax.get_subplotspec().is_last_row()) and (axis in ('y','both')):
            ax.set_xticklabels([])
        if (not ax.get_subplotspec().is_last_col()) and kill_last_xticklabel and (axis in ('x','both')):
            ax.set_xticks(ax.get_xticks().tolist())
            ax.set_xticklabels(ax.get_xticklabels()[:-1]+[''])
        if (not ax.get_subplotspec().is_first_col()) and (axis in ('x','both')):
            ax.set_yticklabels([])
        if not ax.get_subplotspec().is_first_row() and kill_last_yticklabel and (axis in ('y','both')):
            ax.set_yticks(ax.get_yticks().tolist())
            ax.set_yticklabels(ax.get_yticklabels()[:-1]+[''])
        if axlims is not None:
            ax.axis(axlims)




