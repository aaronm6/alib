# aLib

This repository originally started as a collection of tools I put together as I was migrating to Python for
data analysis, and I've just added to it here and there since then.  At some point, some of the functionality 
became useful for a few other people in my research collaboration so I shared it and called it "`aLib`" for 
"Aaron's Library".  I don't believe there are any permanent dependencies on this module, but every once in 
awhile some part of it becomes useful for someone other than myself.  Therefore I'm keeping this repo up-to-date
and public.

Normally I use this library with:
```from aLib import *```
although not everyone likes using the `*` feature with imports.

Below is a list of the submodules contained in `aLib`.  Each module and function/class have fairly thorough
documentation.

- `S.py`:
`S` contains a custom subclass of the python `dict` class, but supports some additional features, such as 
key-access by attribute (e.g. `x['keyname']` but also `x.keyname`, with a tab-completion feature).  The `__repr__`
function is exploited so that a nice text summary of the contents of the object, unlike a bare `dict` object
which just dumps the whole content to `stdout`.

- `astats.py`:
A few statistics-related functions.

- `fits.py`:
Two classes, one for doing chi^2 fits, the other for doing maximum-likelihood fits.  This uses the 
`scipy.optimize` module.

- `hdf.py`:
A collection of a few wrapper functions of the standard `hdf5` read/write built-in module.

- `mathops.p`:
A miscellaneous list of math-related functions.

- `mcmc.py`:
At the moment, this module contains one function which performs a Metropolis-Hastings Markov-chain Monte Carlo.

- `mfile.py`:
Three wrapper functions of `scipy.io` functions, which allow read-write of Matlab `m` files.

- `misc.py`:
General miscellany, much of which is related to plotting.

- `physics.py`:
This module is pretty bare at the moment, and to be honest I haven't used or added to it in awhile.  There is
a dict with physical constants, and a few functions for calculating Compton-scatter quantities.

- `rates`:
This is a library that have functionality for calculating differential rates of both WIMP recoils and 
coherent neutrino-nucleus scatters.

- `yellin`:
Yellin (Phys.Rev.D 66, 032005 (2002)) gives three test statistics that are helpful for calculating upper
limits on a search that potentially contains a background which is unmodeled.  This module has functions
for working with the `p_max` test statistic described in Yellin's paper.

