"""
This module contains some libraries for generating source rates for things like WIMP interactions
and neutrino interactions.

WIMP:
    FQ_Helm
    zeta_McCabe
    WIMPdiffRate
    Er_max_WIMP
    genRandEnergiesWIMP

Neutrino:
    CoNeutRecoilSpect
    Er_max_CNNS
    genRandEnergiesCNNS
"""

#from  rates import WIMP as wimp
#from  rates import Neutrino as nu
"""
from aLib.rates.WIMP import *
from aLib.rates.Neutrino import *
from aLib.rates.EventSims import *
"""
from .WIMP import *
from .Neutrino import *


