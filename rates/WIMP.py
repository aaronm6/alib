"""
WIMP rates module
"""

import numpy as np
from scipy.special import erf
from numpy.random import rand

# Create a class that holds values of the physical constants; units are given as comments
class _pC:
    Gf        = 1.1663787e-11   # MeV^{-2}
    c         = 299792458.      # m/s
    hbar      = 6.58211928e-22  # MeV*s
    eCh       = 1.602176565e-19 # Coulombs; also this many Joules = 1 eV
    sin2thW   = 0.2386          # low-E limit, from PDG
    MeVperAMU = 931.494061      # MeV, from PDG

class TargetParams:
    def __init__(self, tarPropsDict):
        self.Z_N = tarPropsDict['Z_N']
        self.N_N = tarPropsDict['N_N']
        self.A_N = self.N_N + self.Z_N
        self.m_N = tarPropsDict['m_N']
        self.abund_N = tarPropsDict['abund_N']

_builtin_targs = {}
_builtin_targs['Xe'] = {
    'N_N' : np.r_[70., 72., 74., 75., 76., 77., 78., 80., 82.],
    'Z_N' : 54.,
    'm_N' : np.r_[
        123.9058942,
        125.904281,
        127.9035312,
        128.9047801,
        129.9035094,
        130.905072,
        131.904144,
        133.905395,
        135.907214]*_pC.MeVperAMU,
    'abund_N' : np.r_[.09, .09, 1.92, 26.44, 4.08, 21.18, 26.89, 10.44, 8.87]*0.01}
_builtin_targs['Ge'] = {
    'N_N' : np.r_[38., 40., 41., 42., 44.],
    'Z_N' : 32.,
    'm_N' : np.r_[
        69.9242497,
        71.9220789,
        72.9234626,
        73.9211774,
        75.9214016]*_pC.MeVperAMU,
    'abund_N' : np.r_[20.84, 27.54, 7.73, 36.28, 7.61] * 0.01}
_builtin_targs['Si'] = {
    'N_N' : np.r_[14., 15., 16.],
    'Z_N' : 14.,
    'm_N' : np.r_[27.9769271, 28.9764949, 29.9737707]*_pC.MeVperAMU,
    'abund_N' : np.r_[92.23, 4.68, 3.09] * 0.01}
_builtin_targs['Ne'] = {
    'N_N' : np.r_[10., 11., 12.],
    'Z_N' : 10.,
    'm_N' : np.r_[19.9924356, 20.9938428, 21.9913831]*_pC.MeVperAMU,
    'abund_N' : np.r_[90.48, 0.27, 9.25] * 0.01}
_builtin_targs['Ar'] = {
    'N_N' : np.r_[18., 20., 22.],
    'Z_N' : 18.,
    'm_N' : np.r_[35.96754552, 37.9627325, 39.9623837]*_pC.MeVperAMU,
    'abund_N' : np.r_[0.34, 0.06, 99.6] * 0.01}
_builtin_targs['H'] = {
    'N_N' : np.r_[0.,1.],
    'Z_N' : 1.,
    'm_N' : np.r_[1.007825032,2.014101778]*_pC.MeVperAMU,
    'abund_N' : np.r_[(100-.0115),.0115] * 0.01}
_builtin_targs['H1'] = {
    'N_N' : np.r_[0.],
    'Z_N' : 1.,
    'm_N' : np.r_[1.007825032]*_pC.MeVperAMU,
    'abund_N' : np.r_[100.] * .01}
_builtin_targs['D'] = {
    'N_N' : np.r_[1.],
    'Z_N' : 1.,
    'm_N' : np.r_[2.014101778]*_pC.MeVperAMU,
    'abund_N' : np.r_[100.] * .01}
_builtin_targs['He'] = {
    'N_N' : np.r_[1.,2.],
    'Z_N' : 2.,
    'm_N' : np.r_[3.0160293097,4.0026032497]*_pC.MeVperAMU,
    'abund_N' : np.r_[.000137,(100.-.000137)] * 0.01}
_builtin_targs['He4'] = {
    'N_N' : np.r_[2.],
    'Z_N' : 2.,
    'm_N' : np.r_[4.0026032497]*_pC.MeVperAMU,
    'abund_N' : np.r_[100.] * 0.01}
_builtin_targs['He3'] = {
    'N_N' : np.r_[1.],
    'Z_N' : 2.,
    'm_N' : np.r_[3.0160293097]*_pC.MeVperAMU,
    'abund_N' : np.r_[100.] * 0.01}

def get_avail_targets():
    """
    Return a list of element symbols of the available builtin targets
    """
    return list(_builtin_targs)

def _getTargetParams(target):
    """
    Private function, used by several public functions in this module.  Takes a string as
    input that specifies the target, and returns as arrays (or scalars) the following details:
        Z_N    : Number of protons in the target nucleus
        N_N    : Number of neutrons in the target nucleus (this is an array, to account for 
                 all naturally occurring isotopes.
        A_N    : Number of total nucleons in all isotopes of the target nucleus.
        m_N    : Mass of each isotope of the target nucleus (in units of MeV/c^2)
        abund_N: Relative abundance of each isotope (using natural abundances by default).
    """
    #   ------------ <INPUT CHECKING> ------------   #
    if not isinstance(target, str):
        raise TypeError("Input 'target' must be a string.")
    if target not in _builtin_targs:
        raise TypeError("Input 'target' must be an available target")
    #   ------------ </INPUT CHECKING> ------------   #
    if target not in _builtin_targs:
        raise ValueError(
            "Target not recognized.  Please choose from among: {:s}".format(', '.join(_builtin_targs.keys())))
    tParams = TargetParams(_builtin_targs[target])
    return tParams

def FQ_Helm(Q,A):
    """
    Using the Helm form factor from Lewin and Smith. Q is the energy in keV, A 
    is the mass number of the nucleus (dimensionless).
    Inputs:--------------------------------------------------------------------
              Q: ndarray of recoil energy values, in keV.  This should be the 
                 raw kinetic energy of the recoiling nucleus.
              A: The mass number of the target nucleus.
    Outputs:-------------------------------------------------------------------
             ff: The form factor for A, evaluated at Q (ndarray).
    
    2014.06.05
    """
    cparam = 1.23*(A**(1/3))-0.6
    aparam = 0.52
    sparam = 0.9
    j1 = lambda x: np.sin(x)/(x**2) - np.cos(x)/x
    
    rn = np.sqrt((cparam**2)+(7/3)*(np.pi**2)*(aparam**2)-5*(sparam**2))
    q = (6.92e-3)*np.sqrt(A)*np.sqrt(Q)
    #ff = 3*(j1(q*rn)/(q*rn))*np.exp(-((q*sparam)**2)/2)
    #ff = np.zeros_like(Q)
    ff = np.ones_like(Q)
    cutNZ = q*rn != 0
    ff[cutNZ] = 3*(j1(q[cutNZ]*rn)/(q[cutNZ]*rn))*np.exp(-((q[cutNZ]*sparam)**2)/2)
    return ff

def velocities(date=None, standard='baxter'):
    """
    Returns a triplet of velocity magnitudes necessary for evaluating the differential
    recoil spectrum.  Output given as a triplet of three numbers: v0, vesc, v_e
    In units of km/s
    
    The default standard is 'baxter' (which is recommended), which comes from.
        D. Baxter, et al., Eur.Phys.J C (2021) 81
    An older standard, 'lewinsmith' comes from
        J.D. Lewin and P.F. Smith, Astropart. Phys. 6 (1996) 87
    """
    if standard.casefold() not in ('baxter','lewinsmith'):
        raise ValueError("The standards currently available are 'baxter' and 'lewinsmith'")
    if date is not None:
        raise ValueError("Date-specific velocities not yet implemented")
    if standard.casefold() == 'baxter':
        v0 = np.r_[0, 238., 0.]
        vstar = np.r_[11.1, 12.2, 7.3]
        vplus = np.r_[29.2, -0.1, 5.9] # the value that results in a v-dist equal to the yearly average
        v_earth = v0 + vstar + vplus
        v0_mag = np.sqrt((v0**2).sum())
        v_earth_mag = np.sqrt((v_earth**2).sum())
        vesc = 544.
    else:
        v0_mag, vesc, v_earth_mag = 220., 544., 232.
    outDict = {'v_0': v0_mag, 'v_esc': vesc, 'v_earth': v_earth_mag}
    return outDict

def zeta_McCabe(Er_keV, M_W_GeV, M_N_GeV, v_0=238., v_esc=544., v_earth=254., b=0):
    """
    This function returns the value of zeta(E_R), from C. McCabe, PRD 82, 023530 (2010) arXiv: 1005.0579
    
    Inputs:-----------------------------------------------------------------------------------------
             Er: ndarray of recoil energy values, in keV.  This should be the raw kinetic energy of the 
                 recoiling nucleus.
            M_W: The mass of the WIMP, in GeV/c^2
            M_N: Mass of the nucleus in GeV (optional).
            v_0: (kwarg, optional) The characteristic circular velocity at our location in the galaxy, 
                 in km/s.  Default: 238 km/s
          v_esc: (kwarg, optional) The galactic escape velocity at our location in the galaxy, in km/s.
                 Default: 544 km/s
        v_earth: (kwarg, optional) The pecular velocity of the earth, in km/s.  Default: 254 km/s
              b: (kwarg, optional) The beta parameter which controls the exponential cutoff.  Default
                 is zero because it is assumed to be in the newer Baxter paper.
    Outputs:----------------------------------------------------------------------------------------
           zeta: The parameter in McCabe's paper that encompasses the entire velocity integral that goes
                 into the calculation of the differential rate.
    """
    if b not in (0, 1):
        raise ValueError("Input 'b' must be either 0 or 1")
    Er_GeV = Er_keV / (1e6)
    c_kms = _pC.c / (1e3) # convert c from m/s to km/s
    # v_min, in units of c: the minimum velocity a WIMP must have to produce a recoil of energy Er
    v_min_c = ((M_W_GeV + M_N_GeV)/(M_W_GeV*M_N_GeV))*np.sqrt(M_N_GeV*Er_GeV/2)
    # All velocities must be converted to units of c because the zeta function, as defined 
    # in McCabe's appendix B, has dimensions of vel^-1, and it needs to be dimensionless.
    v_0_c = v_0 / c_kms
    v_esc_c = v_esc / c_kms
    v_earth_c = v_earth / c_kms
    
    # McCabe defines further dimensionless velocities, x, in unites of v0
    x_esc = v_esc_c / v_0_c
    x_min = v_min_c / v_0_c
    x_e   = v_earth_c / v_0_c
    
    N = (np.pi**1.5)*(v_0_c**3) * (erf(x_esc) - 4/(np.sqrt(np.pi))*np.exp(-(x_esc**2)) * \
        (x_esc/2 + b*(x_esc**3)/3))
    
    # The numbered cuts refer to the equation number in McCabe's appendix B
    cut4 = x_min <= (x_esc - x_e)
    cut5 = x_min <= (x_esc + x_e)
    cut6 = x_min < (x_e - x_esc)
    cut7 = x_min > (x_e + x_esc)
    
    zeta_out = np.zeros_like(Er_GeV)
    
    zeta_out[cut4] = \
        ((np.pi**1.5)*(v_0_c**2)/2/N/x_e)*(erf(x_min[cut4]+x_e)-erf(x_min[cut4]-x_e) - \
        4*x_e/np.sqrt(np.pi)*np.exp(-(x_esc**2))*(1+b*((x_esc**2)-(x_e**2)/3-(x_min[cut4]**3))))
    
    zeta_out[cut5] = \
        ((np.pi**1.5)*(v_0_c**2)/2/N/x_e)*(erf(x_esc)+erf(x_e-x_min[cut5]) - \
        2/np.sqrt(np.pi)*np.exp(-(x_esc**2))*(x_esc+x_e-x_min[cut5]- \
        (b/3)*(x_e-2*x_esc-x_min[cut5])*((x_esc+x_e-x_min[cut5])**2)))
    
    zeta_out[cut6] = 1/v_0_c/x_e
    
    zeta_out[cut7] = 0.
    
    return zeta_out

def _OLD_zeta_McCabe(Er,A,M_W, MN=None, v_0=220., v_esc=544., v_earth=245., b=0):
    """
    This function returns the value of zeta(E_R), from C. McCabe, PRD 82, 023530 (2010) arXiv: 1005.0579
    
    Inputs:-----------------------------------------------------------------------------------------
             Er: ndarray of recoil energy values, in keV.  This should be the raw kinetic energy of the 
                 recoiling nucleus.
              A: The mass number of the target nucleus.
            M_W: The mass of the WIMP, in GeV/c^2
             MN: Mass of the nucleus in GeV (optional). If none is given, will estimate by multiplying
                 one Dalton (0.931... GeV) by A
            v_0: (kwarg, optional) The characteristic circular velocity at our location in the galaxy, 
                 in km/s.  Default: 220 km/s
          v_esc: (kwarg, optional) The galactic escape velocity at our location in the galaxy, in km/s.
                 Default: 544 km/s
        v_earth: (kwarg, optional) The pecular velocity of the earth, in km/s.  Default: 245 km/s
    Outputs:----------------------------------------------------------------------------------------
           zeta: The parameter in McCabe's paper that encompasses the entire velocity integral that goes
                 into the calculation of the differential rate.
    
    2014.06.05
    """
    # Initial parameter calculations
    v_0 = v_0 / 2.998e5             # convert to units of c
    v_esc = v_esc / 2.998e5         # convert to units of c
    v_earth = v_earth / 2.998e5     # convert to units of c
    if MN is None:
        MN = A * 0.931494061            # calculate the mass of the nucleus in GeV/c^2
    Mr = (M_W * MN) / (M_W + MN)    # calculate the WIMP-nucleus reduced mass
    Er = Er / 1e6                   # convert from keV to GeV
    
    # Calculate the ndarray of v_min, following McCabe's Eq.2, with delta=0
    v_min = np.sqrt(Er*MN/(2*(Mr**2)))
    
    # initialize the vector of xi values
    zeta = np.zeros_like(Er)
    
    # McCabe normalizes each velocity by v_0:
    x_esc   = v_esc / v_0
    x_min   = v_min / v_0
    x_earth = v_earth / v_0
    
    # The normalization constant for the velocity distribution (eq. B3)
    N = (np.pi**(3/2))*(v_0**3) * \
        (erf(x_esc) - 4/np.sqrt(np.pi)*np.exp(-(x_esc**2))*(x_esc/2 + (x_esc**3)/3))
    
    # McCabe gives four conditions regarding the relationship between v_min, and calculates 
    # xi(Er) separately for each
    cut1 = x_min <= (x_esc - x_earth)
    cut2 = (x_min > (x_esc - x_earth)) & (x_min < (x_esc + x_earth))
    cut3 = x_min < (x_earth - x_esc)
    cut4 = x_min >= (x_earth + x_esc)
    # Below are the four conditions that McCabe identifies
    zeta[cut1] = (np.pi**(3/2))*(v_0**2)/2/N/x_earth * \
               (erf(x_min[cut1]+x_earth) - erf(x_min[cut1]-x_earth) - \
               4*x_earth/np.sqrt(np.pi)*np.exp(-(x_esc**2))*(1+x_esc**2-(x_earth**2)/3-x_min[cut1]**3))
    zeta[cut2] = (np.pi**(3/2))*(v_0**2)/2/N/x_earth * \
               (erf(x_esc) + erf(x_earth - x_min[cut2]) - \
               2/np.sqrt(np.pi) * np.exp(-(x_esc**2)) * \
               (x_esc+x_earth-x_min[cut2]-(1/3)*(x_earth-2*x_esc-x_min[cut2])*(x_esc+x_earth-x_min[cut2])**2))
    zeta[cut3] = 1/v_0/x_earth
    zeta[cut4] = 0
    return zeta
    

def WIMPdiffRate(Er, mW=50., sig=1e-45, target='Xe', 
    coupling='isoscalar', v_0=238., v_esc=544., v_earth=254.):
    """
    The differential rate of WIMP recoils in a target, in units of evts/kg/day/keV.  
    Using the formalism of McCabe: C. McCabe, PRD 82, 023530 (2010) arXiv: 1005.0579.  
    This is for 'vanilla' spin-independent, isospin-invariant scattering.
    
    Inputs:---------------------------------------------------------------------------
             Er: ndarray of recoil energy values, in keV.  This should be the raw 
                 kinetic energy of the recoiling nucleus.
             mW: (optional) Mass of the WIMP, in GeV. Default: 50
            sig: (optional) WIMP-nucleon cross section, in cm^2.  Default=1e-45
         target: (optional) Elemental symbol of target Or, a 'TargetParams' object 
                 can be given with the appropriate parameters set.
                 Default: 'Xe'
       coupling: One of four options for pure couplings:
                    'isoscalar': c_p = c_n; rates scales with A^2
                    'isovector': c_p = -c_n; rate scales with (N-Z)^2 (N = number of neutrons)
                    'proton'   : c_n = 0; rate scales with Z^2
                    'neutron'  : c_p = 0; rate scales with N^2
            v_0: (optional) Characteristic circular velocity, in km/s, at our point in 
                 the galaxy. Default: 220 km/s
          v_esc: (optional) Escape velocity at our point in the galaxy, in km/s. 
                 Default: 544 km/s
        v_earth: (optional) Velocity of the earth within the galaxy, in km/s. 
                 Default: 245 km/s
    Outputs:--------------------------------------------------------------------------
             dR: Differential recoil spectrum of WIMPs on nuclei, in units of 
                 events/kg/day/keV
    
    2014.06.05
    """
    if isinstance(target, str):
        Tar = _getTargetParams(target)
    elif isinstance(target, TargetParams):
        Tar = target
    else:
        raise TypeError("kw 'target' must be a str or of class TargetParams")
    # Check that all target params are set:
    TarAnyNone = any([getattr(Tar,item) is None for item in dir(Tar) if not item.startswith('_')])
    assert not TarAnyNone, "All target properties must be set"
    if coupling not in ('isoscalar','isovector','proton','neutron'):
        raise ValueError("Input 'coupling' must be one of the four valid options")
    # ---------- <SET PARAMS IN NAT UNITS> ---------- #
    # rho in GeV/cm^3; convert GeV to MeV, cm^3 to m^3, m^3 to MeV^-3 
    # (note, hbar above is in MeV*s)
    rho = 0.3 * (1e3)* (100.**3.) * (_pC.c**3.) * (_pC.hbar**3.)
    # sig in cm^2; convert cm^2 to m^2, m^2 to MeV^-2
    sig = sig * (1/(100.**2.)) * (1./_pC.c**2.) * (1./_pC.hbar**2.)
    # mW is in GeV, need to convert to MeV
    mW = mW * 1e3
    # mN is the mass of the nucleus.  Need to convert input 'A' to units of MeV
    # mn is the mass of a neutron, in MeV
    mn = 939.565378
    # mu_Wn is the WIMP-neutron reduced mass
    mu_Wn = mW*mn/(mW + mn)
    if coupling == 'isoscalar':
        Q = Tar.A_N
    elif coupling == 'isovector':
        Q = Tar.N_N - Tar.Z_N
    elif coupling == 'proton':
        Q = Tar.Z_N
    elif coupling == 'neutron':
        Q = Tar.N_N
    else:
        raise ValueError("Input 'coupling' must be one of the four valid options")
    v0, vesc, v_e = velocities() # not in natural units, in km/s
    # ---------- </SET PARAMS IN NAT UNITS> ---------- #
    
    # ---------- <CALCULATE THE RATE> ---------- #
    # dR is in units of evts/energy/time/detector_mass
    dR = np.zeros_like(Er)
    #for A_N, N_N, Z_N, m_N, abund_N, Q_N in zip(Tar.A_N, Tar.N_N, Tar.Z_N, Tar.m_N, Tar.abund_N, Q):
    for A_N, N_N, m_N, abund_N, Q_N in zip(Tar.A_N, Tar.N_N, Tar.m_N, Tar.abund_N, Q):
        i_dR = (rho/m_N/mW)*(m_N*sig*(Q_N**2.)/2./(mu_Wn**2.)) * \
             (FQ_Helm(Er,A_N)**2.) * \
             zeta_McCabe(Er, mW/1e3, m_N/1e3, v_0=v_0, v_esc=v_esc, v_earth=v_earth, b=0)
             #zeta_McCabe(Er,A_N,mW/1e3, v_0=v_0, v_esc=v_esc, v_earth=v_earth)
        # convert dR into evts/kg/day/keV
        dR += abund_N * i_dR * ((_pC.c**2.)*3600.*24.)/(_pC.hbar*(1e6)*_pC.eCh*(1e3))
    # ---------- </CALCULATE THE RATE> ---------- #
    return dR

def Er_max_WIMP(mW=50., target='Xe', v_0=220., v_esc=544., v_earth=245.):
    """
    Return the maximum recoil energy, in keV.
    Inputs: -----------------------
           mW: (Optional) WIMP mass, in GeV/c^2. Default: 50 GeV
       target: (Optional) Target nucleus; either a string or a TargetParams object
          v_0: (Optional) Characteristic galactic circular velocity, at our location, km/s.
               Default: 220 km/s
        v_esc: (Optional) Galactic escape velocity at our location, in km/s.
               Default: 544 km/s
    Outputs: ----------------------
       Er_MAX: Maximum energy recoil for the given input parameters.
    """
    if isinstance(target, str):
        Tar = _getTargetParams(target)
    elif isinstance(target, TargetParams):
        Tar = target
    else:
        raise TypeError("Input 'target' must be either of type str or TargetParams")
    c = 2.998e5 # in km/s
    v_WIMPmax = (v_earth + v_esc)/c # max relative WIMP velocity, in Earth's frame
    E_WIMP = .5 * (v_WIMPmax**2) * mW * 1e6 # max WIMP ke in units of keV
    #m_Nuc = 0.931494061 * Tar.A_N # mass of the target nucleus, in GeV
    m_Nuc = Tar.m_N / 1000. # divide by 1000 because m_N is in MeV
    Er_MAX = 4. * E_WIMP * mW*m_Nuc / ((mW + m_Nuc)**2) # max recoil energy, in keV
    return Er_MAX.max()

def genRandEnergiesWIMP(n, mW=50., target='Xe', v_0=220., v_esc=544.):
    """
    Generate random energies according to the expected recoil spectrum of a given WIMP mass.
    Inputs: -----------------------
           n: Number of energies to generate.
          mW: (Optional) Mass of the WIMP in GeV/c^2. Default: 50 GeV
      target: (Optional) Target identity, given as either a string, or a TargetParams object.
         v_0: (Optional) Characteristic circular velocity of objects around the galaxy at our 
              location, in km/s. Default: 220 km/s
       v_esc: (Optional) Galactic escape velocity of objects at our location, in km/s. 
              Default: 544 km/s
    Outputs: ----------------------
          Er: Nuclear recoil energies, in keV.  This is an numpy ndarray.
    
    2014.12.11
    """
    if isinstance(target, str):
        Tar = _getTargetParams(target)
    elif isinstance(target, TargetParams):
        Tar = target
    else:
        raise TypeError("Input 'target' must be either of type str or TargetParams")
    # Calculate maximum possible recoil energy:
    Er_MAX = Er_max_WIMP(mW=mW, target=Tar, v_0=v_0, v_esc=v_esc)
    
    # Generate array of recoil energies from which to sample
    Enr = np.linspace(0, 1.1*Er_MAX,int(2e3))
    dR = WIMPdiffRate(Enr, mW=mW, target=Tar, v_0=v_0, v_esc=v_esc)
    Rcum = dR.cumsum()/dR.sum()  # Energy cumulative distribution function
    cutRange = Rcum < Rcum[-1]
    Rcum = Rcum[cutRange]
    dR = dR[cutRange]
    Enr = Enr[cutRange]
    r_uniform = rand(n)
    Er = np.interp(r_uniform, Rcum, Enr)
    return Er





























