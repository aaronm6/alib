"""
Special functions.  Try "misc.getFuns(mathops)" for a list of functions from the command line.

2014.05.02
"""

import numpy as np
from scipy import special as sp

def cpoiss(x, a, b, L):
    """
    Continuous Poisson function.  NOTE: this is a FUNCTION, not a distribution.  Here, 
    the familiar, discrete Poisson distribution is promoted to a continuous function.  
    Unlike the normal approach, where the factorial (which appears in the denominator of 
    the Poisson distribution) is converted to a gamma function, here a more 
    computationally friendly method is used.  The factorial is replaced by its Stirling 
    approximation.  This is advantageous because the factorial appears only in a ratio; 
    therefore the code must calculate the ratio of two very large numbers.  By using the 
    Stirling approximation, this ratio can essentially be computed directly.
    
    The Stirling approximation is:
        n! ~= n^n * exp(-n) * sqrt(2*pi*n)
    The Poisson distribution is:
        P(k,L) = (L^k) * exp(-L) / (k!)
    The Continuous Poisson is:
        f(x,a,b,L) = a * N * (b*L*e/x)^(x/b) * (2*pi*x/b)^(-1/2)
        N is the normalization constant, such that the function is of unit height when 
        a=1.
        The maximum of this function occurs at x=x_max, where
            x_max = -b / (2 * W(-1/2/L))
        where W() is the Lambert W function (provided by scipy.special.lambertw).
    
    Input parameters:
        x: Independent variable.
        a: Height normalization parameter.
        b: Scale parameter.
        L: Shape parameter.
    
    2014.05.05
    """
    """
    # Define the template function
    f0 = lambda x, b, L: ((b*L*np.exp(1)/x)**(x/b)) / np.sqrt(2*np.pi*x/b)
    # x_max is the value of x that maximizes the function
    x_max = -b / (2*np.real(sp.lambertw(-1/2/L)))
    N = 1/f0(x_max,b,L)
    vOut = np.zeros_like(x)
    #vOut[x!=0] = a * N * f0(x[x!=0],b,L)
    vOut[x>0] = a * N * f0(x[x>0],b,L)
    return vOut
    """
    x0 = -b/(2*np.real(sp.lambertw(-1/2/L)))
    vOut = np.zeros_like(x)
    vOut[x>0] = a*((b*L*np.e)**((x[x>0]-x0)/b)) * \
        ((x0/(x[x>0]**(x[x>0]/x0)))**(x0/b)) * \
        np.sqrt(x0/x[x>0])
    return vOut

def mcpoiss(x, a, b, L):
    """
    Multiple (and constrained) continuous Poisson functions.  See the documentation for 
    'cpoiss', in this same module.  Here, input 'a' is a list (or other iterable) 
    specifying the heights of the various cpoiss components.  Input 'b' will be the same 
    for each component, and input 'L' will be multiplied by the component's iterated 
    number.  So for example:
        mcpoiss(x,[4,5],1,3) = cpoiss(x,4,1,3) + cpoiss(x,5,1,6)
    
    2014.05.22
    """
    try:
        aLen = len(a)
    except:
        raise TypeError("Input a must be iterable, otherwise use 'cpoiss' in this module.")
    vOut = np.zeros_like(x)
    for k, aa in enumerate(a):
        vOut += cpoiss(x,aa,b,(k+1)*L)
    return vOut

def AveBox(inMat_raw, n, r=1):
    """
    Perform a rolling box average filter on a set of data contained in input a, with a 
    box width specified by input 'bWidth'.  A can be a single signal, or an array of 
    signals, but not large than dimension 2.  The box filter is applied along the axis 
    of the array specified by kwarg axis.  For the moment, 'bWidth' must be an odd 
    number.
    
    2014.05.07
    2014.05.15 -- Fixing the problem of adding the wrong number of samples on either end 
                  of the trace.  This is step one, and only implementing for 2-d arrays, 
                  smoothing along the vertical axis.
    """
    inMat = inMat_raw.copy()
    #if (axis != 0) and (axis != 1):
    #    raise ValueError("Input 'axis' must be either 0 or 1")
    if not hasattr(inMat,'ndim'):
        raise ValueError("Input 'inMat' must have an 'ndim' method")
    if inMat.ndim > 2:
        raise ValueError("Input 'inMat' cannot have more than 2 dimensions.")
    
    
    MatStart = inMat.copy()
    ptAddTp = np.floor(n/2)
    ptAddBt = np.floor((n-1)/2)
    
    cutNan = np.isnan(MatStart)
    MatStart[cutNan] = 0.
    cutNan = np.vstack([np.tile(cutNan[0,:],[ptAddTp,1]),cutNan,np.tile(cutNan[-1,:],[ptAddBt,1])])
    NotNanCMSM = (~cutNan).cumsum(0)
    NumDiv = (NotNanCMSM[(n-1):,:] - np.vstack([np.zeros([1,NotNanCMSM.shape[1]]),NotNanCMSM[:-n,:]]))
    for k in range(r):
        #MatStart = double([ repmat(MatStart(1,:),ptAddTp,1) ; MatStart ; repmat(MatStart(end,:),ptAddBt,1)]);
        MatStart = np.vstack([np.tile(MatStart[0,:],[ptAddTp,1]), MatStart, np.tile(MatStart[-1,:],[ptAddBt,1])])
        MatStart = MatStart.cumsum(0)
        #MatStart = (MatStart[(n-1):,:] - np.vstack([np.zeros([1,MatStart.shape[1]]),MatStart[:-n,:]]))/n
        MatStart = (MatStart[(n-1):,:] - np.vstack([np.zeros([1,MatStart.shape[1]]),MatStart[:-n,:]]))/NumDiv
    return MatStart

def randnCov(covmat, mu=None, n=1, U=None, eVals=None):
    """
    Generate a set of correlated random numbers that are drawn from a multidimensional Gaussian 
    with a specified covariance matrix.  The code works by first calculating a unitary operator 
    that transforms from the input basis (in which the random numbers are correlated) into the 
    eigenbasis of the covariance matrix.  In this eigenbasis, the covariance matrix is diagonal, 
    all random numbers in the set are uncorrelated, and the eigenvalues give the variances of 
    these uncorrelated random numbers.  Uncorrelated random numbers are generated in this basis 
    and then transformed back to the original basis, thus introducing the desired correlations 
    described by the given covariance matrix.
    
    Inputs:
        covmat: Covariance matrix that expresses all variable-variable correlations.  Must be
                a numpy array (not list, not a numpy matrix), must be square.  If numpy chokes
                when trying to calculate the eigenvalues and eigenvectors, so will this 
                function :-)
            mu: (optional) The means of each correlated random number.  Default is 0 for each.
                If non-zero means are desired, this input must be given as a 1-D numpy array 
                with length equal to the rank of the covariance matrix.
             n: (optional) Number of sets of these random numbers to generate.
             U: (optional) The unitary operator that diagonalizes the covariance matrix. In cases
                where this function is called repeated times using the same covariance matrix,
                you might want to calculate U outside the function so that the same repeated 
                eigenval/vect calculation doesn't have to be repeated over and over again.
         eVals: (optional) Eigenvalues of covmat.  Optional for the same reason U is.
                U AND EVALS MUST BOTH BE SPECIFIED OR NEITHER.
    Outputs:
      randsOut: Array of correlated random numbers.  If n=1, the output is a regular numpy 1-D 
                array, of length given by the dimension of the covariance matrix.  If n>1, this
                output is a 2-D array, with n rows, and number of columns given by the dimension 
                of the covmat.  In other words, each row is a correlated set of random numbers.  
                Each row is an independent set of these random numbers.
    
    2016.03.23 
    """
    # ------- <Input checking> ------- #
    assert isinstance(covmat, np.ndarray), "Covariance matrix must be a numpy array."
    assert len(covmat.shape)==2, "Input 'covmat' must be a 2-D matrix."
    assert covmat.shape[0]==covmat.shape[1], "Input 'covmat' must be a square matrix."
    if mu is not None:
        assert hasattr(mu, '__len__'), "Input 'mu' must be an array"
        if isinstance(mu,list):
            mu = np.array(mu)
        assert isinstance(mu, np.ndarray), "Input 'mu' must be a list or numpy array"
        assert len(mu.shape) == 1, "Input 'mu' must be 1-dimensional."
        assert len(mu)==covmat.shape[0], "If input 'mu' is given, its length must be equal to " + \
            "the rank of the covariance matrix."
    # ------- </Input checking> ------- #
    
    # Number of correlated random numbers (i.e. the dimension of the cov. matrix)
    numRands = covmat.shape[0]
    
    # The user can provide their own unitary transformation to save on computation time
    # in the case of repeated calls with the same covmat. Otherwise, it is calculated here.
    if U is None: 
        # Find the eigenvalues and eigenvectors of the covariance matrix
        eVals, eVects = np.linalg.eig(covmat)
        
        # Calculate the unitary operator that transforms from the input basis, into the
        # covariance matrix's eigenbasis.  Since only its transpose is used later on, I just
        # calculate the transpose-transformation directly.
        #U = eVects.T
        U_transpose = eVects
    else:
        U_transpose = U.T
    
    assert eVals is not None, "You must also specify the eigenvectors."
    
    if n==1:
        # Generate random numbers in the diagonal basis (no correlations, obviously)
        randsDiag = np.random.randn(numRands)*np.sqrt(eVals)
        # Rotate the random numbers into the original basis.
        randsOut  = np.dot(U_transpose, randsDiag)
        if mu is not None:
            randsOut += mu
    else:
        #randsDiag = np.random.randn(n,numRands)*np.tile(np.sqrt(eVals),[n,1])
        #randsOut  = (np.dot(U_transpose, randsDiag.T)).T
        randsDiag = np.random.randn(numRands,n)*np.tile(np.c_[np.sqrt(eVals)], [1,n])
        randsOut  = (np.dot(U_transpose, randsDiag)).T
        if mu is not None:
            randsOut += np.tile(mu,[n,1])
    return randsOut

def genBasis(n):
    """
    Generate a random, orthnormal set of basis vectors in n-dimensional space.  The method starts
    by generating an nxn matrix of unit-variance Gaussian random numbers, and treats each column
    as the components of a vector.  The first column is taken to be the first basis vector, and
    then the rest are generated via the Gram-Schmidt method.  The mutually orthagonal vectors are
    then normalized.
    
    Input:
                 n: The dimensionality of the space in which the vectors will be generated.  
                    Also, this is the number of vectors that will be generated (obviously).
    Output:
        basisVects: An nxn array, in which the columns are to be interpreted as the basis 
                    vectors.
    
    2016.03.25
    """
    randVects = np.random.randn(n,n)
    basisVects = np.empty_like(randVects)
    
    # I repeat the process twice, in case any two vectors happened to be very nearly parallel to 
    # begin with, and they might not be completely orthogonal after the first iteration of Gram-
    # Schmidting due to floating-point precision.
    for k0 in range(2):
        # The for-loop handles the Gram-Schmidt stuff
        for k in range(n):
            if k==0:
                basisVects[:,k] = randVects[:,k]
            else:
                projCoeffs = np.dot(randVects[:,k],basisVects[:,:k])/(basisVects[:,:k]**2).sum(0)
                projections = (np.tile(projCoeffs,[n,1])*basisVects[:,:k]).sum(1)
                basisVects[:,k] = randVects[:,k] - projections
        
        # They are mutually orthogonal, but next they need to be normalized.
        basisVects = basisVects/np.tile(np.sqrt((basisVects**2).sum(0)),[n,1])
        randVects = basisVects.copy()
    return basisVects

def triangle_orientation(p1, p2, p3):
    """
    Determines the triangular orientation of three points, in the order given.  "Orientation"
    here means, as one steps through the three points and back to the first, is the path 
    following a clockwise or counterclockwise direction.  This function is needed in order
    to determine if two line segments intersect, which is in turn needed to determine whether
    or not a point is inside an arbitrary polygon.
    
    inputs are all 2-element ndarrays.
    one can be a 2xn array
    The order matters
    
    output:  (respecting the right-hand-rule)
         1: counter clockwise
         0: collinear
        -1: clockwise
    
    2018.05.27
    """
    numEvents = None
    num2Darrays = 0
    for point in (p1, p2, p3):
        assert isinstance(point, np.ndarray), "Points must be numpy arrays."
        assert point.ndim <= 2, "Inputs cannot have more than two dimensions."
        if point.ndim ==2:
            assert point.shape[0] == 2, "The input with many points cannot have more than two rows"
            numEvents = point.shape[1]
            num2Darrays += 1
    assert num2Darrays <= 1, "At most one input can be 2D."
    if numEvents:
        vect1 = np.c_[p2] - np.c_[p1]
        vect2 = np.c_[p3] - np.c_[p2]
    else:
        vect1 = p2 - p1
        vect2 = p3 - p2
    v1_x_v2 = vect1[0]*vect2[1] - vect2[0]*vect1[1]
    outPut = np.sign(v1_x_v2)
    return outPut

def segments_intersect(p1, p2, q1, q2):
    """
    Determines whether two line segments (not full lines) intersect.  Relies on looking at 
    different triangle orientations of the four points that define the two line segments.
    
    Inputs:
    p1 and p2 are the points that define the first line segment.
    q1 and q2 are the points that define the second line segment.
    
    all inputs are 2-element ndarrays.  One of them can be 
    2xn.
    
    Output: boolean array of length n: True means they intersect
    
    2018.05.27
    """
    # No input handling here, because the function 'triangle_orientation' essentially handles that.
    
    # determine the triangle orientations of relevant combinations
    tor_p1p2q1 = triangle_orientation(p1, p2, q1)
    tor_p1p2q2 = triangle_orientation(p1, p2, q2)
    tor_q1q2p1 = triangle_orientation(q1, q2, p1)
    tor_q1q2p2 = triangle_orientation(q1, q2, p2)
    
    tor_p1p2_diff = (tor_p1p2q1 + tor_p1p2q2) == 0
    tor_q1q2_diff = (tor_q1q2p1 + tor_q1q2p2) == 0
    
    outPut = tor_p1p2_diff & tor_q1q2_diff
    
    return outPut

def polygon_cut(x, y, polypts):
    """
    Inputs:
              x: 1-dim ndarray of x-values of points (to test whether they are 
                 in the polygon).
              y: 1-dim ndarray of y-values of points (to test whether they are 
                 in the polygon).
        polypts: 2-dim ndarray (2xn) of [[xvalues],[yvalues]] of the vertices of 
                 the polygon.
    
    Output:
        in_poly: 1-dim ndarray of type bool, same size and shape as the x and y 
                 inputs.  True if the point described by x, y is in the polygon,
                 False otherwise.
    
    2018.05.27
    """
    for inArg in (x, y, polypts):
        assert isinstance(inArg, np.ndarray), "All inputs must be numpy arrays"
    
    assert x.ndim == 1, "Input 'x' must be a 1-dimensional array"
    assert y.ndim == 1, "Input 'y' must be a 1-dimensional array"
    assert polypts.ndim == 2, "Input 'polypts' must have two dimensions"
    assert polypts.shape[0] == 2, "First dimension of input 'polypts' must have 2 elements"
    
    # First, determine the min,max x and y values of the polygon.  This will 
    # facilitate doing a cut on the bounding rectangle of the polygon, which is 
    # much easier to decide on than the points inside the bounding rectangle.
    x_poly_max = polypts[0,:].max()
    x_poly_min = polypts[0,:].min()
    y_poly_max = polypts[1,:].max()
    y_poly_min = polypts[1,:].min()
    
    in_poly = np.ones_like(x, dtype=bool)
    
    # Efficiently cut out points that fall outside of the bounding rectangle:
    in_poly[x < x_poly_min] = False
    in_poly[x > x_poly_max] = False
    in_poly[y < y_poly_min] = False
    in_poly[y > y_poly_max] = False
    
    # Now choose an endpoint of the line segment that all points inside the box
    # will point to.  Endpoint is far outside the box
    ptOut_x = x_poly_max + (x_poly_max - x_poly_min)*5
    ptOut_y = y_poly_max + (y_poly_max - y_poly_min)*5
    
    numPolyVerts = polypts.shape[1]
    
    if (polypts[0,0] == polypts[0,-1]) and (polypts[1,0] == polypts[1,-1]):
        numPolyVerts -= 1
    else:
        polypts = np.hstack([polypts, np.c_[polypts[:,0]]])
    
    numIntersects = np.zeros(in_poly.sum(), dtype=int)
    for k in range(numPolyVerts):
        # 'cut_i' is the cut that specifies whether the input points intersect
        # the k'th side of the polygon on their way to the point far outside
        # the polygon.
        cut_i = segments_intersect(np.vstack([x[in_poly],y[in_poly]]),
                           np.r_[ptOut_x, ptOut_y],
                           np.r_[polypts[0,k], polypts[1,k]],
                           np.r_[polypts[0,k+1], polypts[1,k+1]])
        numIntersects[cut_i] += 1
    
    # Now tag points which intersect an odd number of sides of the polygon on their way
    # out to a point definitely outside the polygon
    oddIntersects = (numIntersects % 2) == 1
    in_poly[in_poly] = oddIntersects
    return in_poly

def alinspace(start, end, n=50):
    """
    Like numpy's 'linspace', but builds a 2-D array, because it can take an array
    of starts and ends.
    
    'start' and 'end' inputs must be numpy arrays of the same length, 1-dimensional.
    'n' must be an integer (not an array of integers).  All range-arrays must have
    the same number, since the output is a 2-D array.
    
    Output: A 2-D array; the first row is identical to input 'start', the last row
            identical to 'end'.  Each column is an array.  That is:
    
    >> alinspace(start, end)[:,k]
    
    is the same as
    
    >> linspace(start[k], end[k])
    
    2018.09.13
    """
    
    #---------<input checking>
    assert isinstance(start, np.ndarray), "Input 'start' must be a numpy array."
    assert isinstance(end, np.ndarray), "Input 'end' must be a numpy array."
    assert isinstance(n, int), "Input 'n' must be an integer."
    assert n > 1, "Input 'n' should really be greater than 1."
    assert start.size == end.size, "Inputs 'start' and 'end' must be the same size."
    #---------<-input checking>
    
    steps = (end - start) / (n-1)
    tArray = np.tile(steps,[n,1])
    tArray[0,:] = start
    outArray = tArray.cumsum(0)
    return outArray


def root_search(fun, x_l, x_r, b, *args, alg='regula', tol_b=1e-10, tol_x=1e-10, 
    max_iter=100, verbosity=1, **kwargs):
    """
    Root finding, with either the "regula falsi" or "bisection" algorithms. This 
    is useful if we have a function, f(x), and we wish to approximate the value of 
    x at which f(x) crosses some threshold b.  We start by assuming we have 
    x-values 'x_l' and 'x_r' which bracket the true value of x=x_b, at which 
    f(x_b) = b.  The regula falsi algorithm works by drawing the line given from 
    point [x_l,f(x_l)] to point [x_r,f(x_r)], and then finding the value of x_0 where 
    that line crosses b.  The bisection method is simpler, in which x_0 is simply 
    taken as the midpoint between x_l and x_r.  Once x_0 is selected, the two 
    algorithms are the same.  If f(x_0) is on the same side of b as f(x_l), then the 
    value of x_l is replaced by x_0; otherwise, the value of x_r is replaced by 
    x_0.  The process is then repeated indefinitely, to any desired precision.  
    Either it stops when abs(b-f(x_0)) falls below some value, or abs(x_r-x_l) 
    falls below some value.  This works well if f(x) has a step-like discontinuity 
    and we wish to find where that discontinuity occurs.
    
    Additionally, b can be a function, and one is searching for the point at which
    'b' and 'fun' cross.
    
    Very roughly speaking, if x_r-x_l is of the same order as x_b, then the 
    number of iterations is similar to the number of significant digits of 
    precision of the estimate.
    
    NOTE: scipy.optimize.bisect is an implementation of the bisection method; I
          tried this and it appears to be faster than this function on the level
          of x1.5-x2.  scipy.optimize has other methods as well, but I haven't
          tried them; they can all be accessed with the scipy.optimize.root_scalar
          method.
    
    Inputs:
           fun: The function to be evaluated
           x_l: The left edge of the initial bracketing window.
           x_r: The right edge of the initial bracketing window.
             b: The value of the function that is to be interpolated.  This input 
                can be a function, in which case the algorithm finds the crossing
                point.  If 'b' is a function, 'alg' input must be 'bisection'
         *args: Any additional arguments that are needed by the function
           alg: The algorithm of root finding to be used.  Choices are 'regula' 
                and 'bisection'.
         tol_b: When |b-f(x_0)| falls below this value, the iterations stop.
         tol_x: When |x_r-x_l| falls below this value, the iterations stop.\
      max_iter: Maximum allowed number of iterations
     verbosity: Default: 1.  0 means print nothing.  1 means print warning when
                max_iter has been reached. 2 means print the number of iterations.
      **kwargs: Any additional keyword arguments needed by the function.
    Outputs:
           x_b: The estimate of the value of x for which f(x_b) = b (if b is a number)
                or for which f(x_b) = b(x_b) (if b is a function)
    
    2018.09.15
    2018.10.04 -- Added the possibility to give input 'b' as  function. Also allowing 
                  the user to chose the algorithm to use.
    """
    #-------<input checking>-------#
    assert hasattr(fun, '__call__'), "Input 'fun' must be callable."
    assert isinstance(x_l, int) or isinstance(x_l, float), "x_l must be just a number"
    assert isinstance(x_r, int) or isinstance(x_r, float), "x_r must be just a number"
    assert isinstance(b, int) or isinstance(b, float) or hasattr(b,'__call__'), \
        "Input 'b' must be a number or a function"
    assert isinstance(max_iter, int), "max_iter must be an integer"
    if hasattr(b, '__call__'):
        flag_b_func = True
    else:
        flag_b_func = False
    if flag_b_func:
        assert (fun(x_l,*args,**kwargs)-b(x_l))*(fun(x_r,*args,**kwargs)-b(x_r)) < 0., \
            "f(x_l) and f(x_r) must be on opposite sides of b."
    else:
        assert (fun(x_l,*args,**kwargs)-b) * (fun(x_r,*args,**kwargs)-b) < 0., \
            "f(x_l) and f(x_r) must be on opposite sides of b."
    assert alg in ['regula','bisection'], "Input 'alg' must be either 'regula' or 'bisection'"
    if flag_b_func and alg == 'regula':
        print("Warning: 'regula' method cannot be used if b is a function. Switching to bisection.")
        alg = 'bisection'
    #-------</input checking>-------#
    f_l = fun(x_l, *args, **kwargs)
    f_r = fun(x_r, *args, **kwargs)
    b_l = b(x_l) if flag_b_func else b
    p_l = np.sign(f_l-b_l)
    x_b = x_l
    f_b = fun(x_b, *args, **kwargs)
    b_b = b(x_b) if flag_b_func else b
    get_xEst_reg = lambda x_l, x_r, f_l, f_r, b_l: (b_l-f_l)*(x_r-x_l)/(f_r-f_l) + x_l
    get_xEst_bis = lambda x_l, x_r, *args: .5*(x_l + x_r)
    get_xEst = get_xEst_reg if alg=='regula' else get_xEst_bis
    k = 0
    while (abs(f_b-b_b) > tol_b) and (abs(x_r-x_l) > tol_x) and (k < max_iter):
        x_b = get_xEst(x_l, x_r, f_l, f_r, b_l)
        f_b = fun(x_b, *args, **kwargs)
        b_b = b(x_b) if flag_b_func else b
        if ((f_b-b_b)*p_l) > 0.:
            x_l = x_b
            f_l = fun(x_l, *args, **kwargs)
        else:
            x_r = x_b
            f_r = fun(x_r, *args, **kwargs)
        k += 1
    if (k >= max_iter) and (verbosity >= 1):
        print("Warning: maximum number of iterations reached")
    if (verbosity >= 2):
        print("Number of iterations: {:d}".format(k))
    return x_b

def points2line(x1,y1,x2,y2):
    """
    Gives the slope and y-intercept of the line defined by the input points.
    Output: (slope, y-intercept)
    
    2016.05.27
    """
    m = (y2-y1)/(x2-x1)
    b = y1 - m*x1 
    return m,b

def points2parabola(x1,y1,x2,y2,c):
    """
    Given two points, and an offset coefficient 'c', calculates the first
    two coefficients of the quadratic function that goes through those
    two points.  That is, if the function, f(x), is:
        f(x) = a*x^2 + b*x + c
    then the value of c is given as an input, and this function returns a and b.
    Output: (a, b)
    
    2020.04.30
    """
    m = (y2 - y1) / (x2 - x1)
    a = (c + m*x1 - y1) / (x1 * x2)
    b = m - a * (x1 + x2)
    return a, b

def points3parabola(x1,y1,x2,y2,x3,y3):
    """
    Given three points, this function calculates the coefficients on the 
    quadratic function that goes through these three points.  If the quadratic
    is expressed as:
        f(x) = a*x^2 + b*x + c
    This returns a length-3 tuple: (a, b, c)
    
    2020.04.30
    """
    xvec = np.r_[x1, x2, x3]
    xmat = np.hstack([np.c_[xvec**2],np.c_[xvec],np.c_[ones(3)]])
    ycol = np.c_[[y1, y2, y3]]
    abc  = np.dot(np.linalg.inv(xmat), ycol)
    a, b, c = abc.flatten()
    return a, b, c

def primes(n):
    """
    Returns a list containing the prime factors of integer input n
    """
    #-------<input checking>-------#
    assert isinstance(n,int) or (n % int(n) == 0), "Input n must be an int"
    if not isinstance(n,int):
        n = int(n)
    #-------</input checking>-------#
    
    prList = []
    # Print the number of two's that divide n
    while n % 2 == 0:
        prList.append(2)
        n = int(n / 2)
        
    # n must be odd at this point
    # so a skip of 2 ( i = i + 2) can be used
    #for i in range(3,int(np.sqrt(n))+1,2):
    for i in range(3,int(np.sqrt(float(n)))+1,2):
        # while i divides n , print i ad divide n
        # if n is already 1, no need to try to divide by any other numbers
        if n == 1:
            break
        while n % i== 0:
            prList.append(i)
            n = int(n / i)
            
    # Condition if n is a prime
    # number greater than 2
    if n > 2:
        prList.append(n)
        
    return prList

def multinom(k):
    """
    Calculate the multinomial coefficient (does not exist in scipy for some reason).
    k is like a histogram; this coefficient gives the number of ways to distribute 
    the contents of bin to get those hits.  For example, r_[1,1] has two ways,
    i.e. (a,b) and (b,a).  r_[1,2] has three ways, i.e. (a, b+c), (b, a+c), (c, a+b).
    The formula is:
    factorial(n) / factorial(k[0]) / factorial(k[1]) / factorial(k[2]) / ...
    where n = k.sum()
    """
    assert isinstance(k, np.ndarray), "input must be a numpy array"
    return sp.binom(k.cumsum(),k).prod()

def _Rx(gam):
    r_mat = np.array([
        [1, 0, 0],
        [0, np.cos(gam), -np.sin(gam)],
        [0, np.sin(gam), np.cos(gam)]])
    return r_mat
def _Ry(ph):
    r_mat = np.array([
        [np.cos(ph), 0, np.sin(ph)],
        [0,1,0],
        [-np.sin(ph), 0, np.cos(ph)]])
    return r_mat
def _Rz(th):
    r_mat = np.array([
        [np.cos(th), -np.sin(th), 0],
        [np.sin(th), np.cos(th), 0],
        [0,0,1]])
    return r_mat

def compound_rotation_matrix(axis_order, rot_angles):
    """
    Turn a series of rotations about x, y, and z axes into a single rotation matrix.
    INPUTS:
    axis_order is a string like "yzxzx" where the first letter in the string corresponds
        to the first rotation (not the reversed order like in applying the rotation operators).
    rot_angles is an ndarray of the rotation angles (in radians) corresponding to each of
        the axes given in 'axis_order'
    
    OUTPUTS:
    R : the compound rotation matrix
    """
    # NEED INPUT CHECKING
    R = np.identity(3)
    for ax, th in zip(axis_order.lower()[::-1], rot_angles[::-1]):
        if ax == 'x':
            f_rot = _Rx
        elif ax == 'y':
            f_rot = _Ry
        elif ax == 'z':
            f_rot = _Rz
        else:
            raise ValueError("Input 'axis_order' must have only x's, y's, and z's")
        R = f_rot(th).dot(R)
    return R

def compound_rotation(axis_order, rot_angles):
    """
    Turn a series of rotations about x, y, and z axes into a single rotation about a
    single axis.  This finds that angle and that axis.
    INPUTS:
    axis_order is a string like "yzxzx" where the first letter in the string corresponds
        to the first rotation (not the reversed order like in applying the rotation operators).
    rot_angles is an ndarray of the rotation angles (in radians) corresponding to each of
        the axes given in 'axis_order'
    
    OUTPUTS:
    com_angle: the single angle equivalent to the compound rotation
    com_axis:  the single axis of rotation equivalent to the compound rotation
    """
    R = compound_rotation_matrix(axis_order, rot_angles)
    eVals, eVects = np.linalg.eig(R)
    #real_eval = (np.imag(eVals)**2) < 1e-10 # 1e-10 might be too big, though probably not
    real_evals = eVals.imag**2 < 1e-10
    if real_evals.sum() > 1:
        print(f"{rot_angles}")
        raise ValueError("Rotation matrix has more than one eigenvalue")
    com_axis = eVects[:, real_evals].real
    idx_real = np.nonzero(real_evals)[0][0]
    idx_ang = (idx_real + 1)%3
    #com_angle = np.log(eVals[~real_evals][0]).imag
    com_angle = np.log(eVals[idx_ang]).imag
    return com_angle, com_axis

def rotation_axis_matrix(c_angle, c_axis):
    """
    Given a rotation angle and an arbitrary axis of rotation (defined in x,y,z coordinates),
    construct the rotation matrix in the x,y,z basis.
    
    INPUTS:
    c_angle: angle of the rotation in radians (obeying right-hand-rule). Scalar.
    c_axis: 3x1 matrix (i.e. a 2D column vector), the coordinates of the arbitrary 
            axis of rotation
    
    OUTPUTS:
    R: transformation matrix (in the basis in which the c_axis is given) that performs
       the rotation about c_axis.
    """
    th_y, = -np.arctan2(c_axis[0], c_axis[2])
    c_ax_1 = _Ry(th_y).dot(c_axis)
    th_x, = np.arctan2(c_ax_1[1], c_ax_1[2])
    R = _Ry(-th_y).dot(_Rx(-th_x).dot(_Rz(c_angle).dot(_Rx(th_x).dot(_Ry(th_y)))))
    return R


