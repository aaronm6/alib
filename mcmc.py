"""
This module provides functionality for performing Markov-Chain Monte Carlos.

2017.12.23
"""

from __future__ import division
from __future__ import print_function
import numpy as np
from scipy import rand, randn
import types

def mh(logL, fitParams, *args, **kwargs):
    """
    Performs a generic Metropolis-Hastings Markov Chain Monte Carlo fit.  Proposal densities are 
    modeled by Gaussian distributions, with lower and upper bounds and sigma defined by the 
    input parameters.
    
      inputs: 
         logL: Function which returns the LOG of the Likelihood.  This function must take N  parameters (in an 
               array) as the first input, the same number as given in stVals, bndLow, and sig_jumps; more inputs
               can be accepted, which are optional: they are passed as the *args and **kwargs inputs of mh(..)
    fitParams: A dict with fields that specify the parameters of how the fit is excecuted.  Fields listed as 
               "optional" can be left out of the structure.  Mandatory and optional fields are:
          stVals: The starting values of the fit parameters to use in the MHMCMC. numpy ndarray
       sig_jumps: The sigma (for each parameter) to use in the proposal density when choosing a
                  proposed location in parameter space for the next jump. numpy ndarray
          bndLow: (optional) The lower bounds on the fit parameters.  If no bound is needed on one or any of the
                  parameters, give it as "-inf" (negative infinity).  Default: -inf
         bndHigh: (optional) The upper bounds on the fit parameters.  If no bound is needed on one or any of the
                  parameters, give it as "inf" (positive infinity).  Default: +inf
           nIter: (optional) Number of iterations to use. Must be an int.  Default: 1e3
          priors: (optional) Python list with two-element lists/tuples denoting (mu,sig) pairs for Gaussian priors 
                  only.  Give empty entries for uniform priors. Ex: [[],[3.,1.],[]] for a gaussian prior on the 
                  second element, with mu=3, sig=1.
     constraints: (optional) A function or list of functions that apply further constraints on the free parameters
                  of likelihood function, beyond the min and max defined by 'bndLow' and 'bndHigh'.  The function(s)
                  must take a single input parameter which is an array of length equal to that of 'stVals'.  The 
                  function(s) must return a single boolean value: True if the constraints are met by the propsed
                  combination of free parameters, False if not.
     shoutPeriod: (optional) A "shout" is where the function sends the iteration number to the stdout.  When the 
                  iteration number is a multiple of the shoutPeriod, it will give a shout.  To disable shouts, 
                  give shoutPeriod = 0.  Default: 100
    args, kwargs: (optional) Extra inputs that are simply passed on to the log-likelihood function
    
     outputs:
    out_params: An array of (number of params + 1)x(nIter) of each accepted jump.  The first
                dimension is of size (number of params + 1) because the value of the posterior
                PDF (Likelihood * priors) for those values of the input parameters is given as 
                the last row.
    
    See also MH_ICMC, MH_process, MP_RLS  (not implemented yet)
    """
    # --------------- INPUT CHECKING --------------- #
    assert isinstance(logL, types.FunctionType), "First input must be a function."
    #assert isinstance(fitParams,dict), "Input 'fitParams' must be a dictionary or subclass thereof."
    assert hasattr(fitParams,'keys'), "Input 'fitParams' must be a dict or subclass thereof."
    # There's probably another way to check if the object is of class or subclass of dict.  Eg
    #assert (fitParams.__class__ == dict) or np.any([fPClass==dict for fPClass in fitParams.__class__.__bases__]), \
    #    "Input 'fitParams' must be a dictionary or subclass thereof."
    # but this handles only one level of nested subclasses.  Probably the un-commented method (i.e. check for 
    # 'keys' attribute) is the best way.
    
    assert np.all([aa in fitParams.keys() for aa in ['stVals','sig_jumps']]), \
        "Input fitParams must contain AT LEAST the fields 'stVals' and 'sig_jumps'"
    
    # convert any of these fields to numpy arrays if they are given as lists
    for fKey in ['stVals','sig_jumps','bndLow','bndHigh']:
        if fKey in fitParams.keys():
            if isinstance(fitParams[fKey], list):
                fitParams[fKey] = np.array(fitParams[fKey])
    
    if 'nIter' not in fitParams.keys():
        fitParams['nIter'] = int(1e3)
    
    if ('bndHigh' not in fitParams.keys()) or (fitParams['bndHigh'].size == 0):
        fitParams['bndHigh'] = np.full(fitParams['stVals'].shape, np.inf, dtype=float)
    if ('bndLow' not in fitParams.keys()) or (fitParams['bndLow'].size == 0):
        fitParams['bndLow'] = np.full(fitParams['stVals'].shape, -np.inf, dtype=float)
    
    if 'constraints' not in fitParams.keys():
        fitParams['constraints'] = []
    
    if not isinstance(fitParams['constraints'],list):
        fitParams['constraints'] = [fitParams['constraints']]
    
    assert (fitParams['stVals'].shape == fitParams['bndLow'].shape) and \
           (fitParams['stVals'].shape == fitParams['bndHigh'].shape) and \
           (fitParams['stVals'].shape == fitParams['sig_jumps'].shape), \
           "The input arrays 'stVals', 'sig_jumps', 'bndLow', and 'bndHigh' must have the same shape."
    #assert (len(fitParams['stVals']) == len(fitParams['bndLow'])) and \
    #       (len(fitParams['stVals']) == len(fitParams['bndHigh'])) and \
    #       (len(fitParams['stVals']) == len(fitParams['sig_jumps'])), \
    #       "The input arrays 'stVals', 'sig_jumps', 'bndLow', and 'bndHigh' must have the same shape."
    
    if 'priors' not in fitParams.keys():
        fitParams['priors'] = [[] for aa in range(len(fitParams['stVals']))]
    
    #assert fitParams['priors'].shape == fitParams['stVals'].shape, \
    assert len(fitParams['priors']) == len(fitParams['stVals']), \
        "The array/list of priors must have the same length as the other inputs (e.g. stVals)"
    
    if 'shoutPeriod' not in fitParams.keys():
        fitParams['shoutPeriod'] = 100
    # --------------- /INPUT CHECKING --------------- #
    
    # -------- allocate mem for output array -------- #
    out_params = np.empty([len(fitParams['stVals'])+1, fitParams['nIter']], dtype=float)
    # -------- /allocate mem for output array -------- #
    
    # ------ Set initial values and Likelihood ------ #
    fVals = fitParams['stVals'] # fVals is the vector of parameter values
    L = logL(fVals, *args, **kwargs)
    Plog = 0. # Plog is the log of the priors
    for k in range(len(fitParams['priors'])):
        if len(fitParams['priors'][k]) > 0:
            Plog = Plog - ((fVals[k] - fitParams['priors'][k][0])**2)/2/(fitParams['priors'][k][1]**2) - \
                np.log(fitParams['prior'][k][1])
    
    # ------ /Set initial values and Likelihood ------ #
    
    # --- Start iterating and sampling the Likelihood ---#
    for k in range(fitParams['nIter']):
        # shout
        if (k%fitParams['shoutPeriod']) == 0:
            print("k = {:d}".format(k))
        
        # Add fVals of the previous iteration to the output array 
        out_params[:-1,k] = fVals
        # Add the log-likelihood of the previous iteration to the last row of the output array
        out_params[-1,k] = L + Plog
        
        # Pick a new set of proposal values
        propVals = fitParams['sig_jumps']*randn(*fitParams['sig_jumps'].shape) + fVals
        
        # Check if the proposed values are valid
        validProps = ((propVals>=fitParams['bndLow'])&(propVals<=fitParams['bndHigh'])).all()
        
        # bit about more complex constraints
        validProps = validProps and np.all([tFunc(propVals) for tFunc in fitParams['constraints']])
        
        # If the proposed values are not valid, they are rejected
        if validProps:
            L_prop = logL(propVals, *args, **kwargs)
            Plog_prop = 0.
            for kp in range(len(fitParams['priors'])):
                if len(fitParams['priors'][kp]) > 0:
                    Plog_prop = ((Plog_prop - propVals[kp]-fitParams['priors'][kp][0])**2) / 2 / \
                        (fitParams['priors'][kp][1]**2) - np.log(fitParams['priors'][kp][1])
            
            L_ratio = np.exp(L_prop - L)
            P_ratio = np.exp(Plog_prop - Plog)
            
            a = rand()
            if a<(L_ratio * P_ratio):
                fVals = propVals
                L = L_prop
                Plog = Plog_prop
    
    # --- /Start iterating and sampling the Likelihood ---#
    return out_params

