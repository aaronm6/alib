import numpy as np

C = dict() # constants
C['e'] = 1.6e-19  # charge of electron in Coulombs
C['h'] = 1.054571628e-34 # h_BAR in Js
C['c'] = 2.9979e8  # speed of light in m/s
C['m'] = 9.11e-31  # mass of the electron in kg
C['mc2'] = 511. # mass of electron in keV
C['r'] = C['h']/(C['m']*C['c'])
C['alph'] = 7.297e-3  # fine structure constant

def Compton_Energy(E_gamma, th):
    """
    Calculates the energy of the recoiling electron from a Compton scatter with a gamma ray
    with energy E_gamma and scattering angle th.
    IMPORTANT:
    E_gamma must be in units of keV
    th must be in radians
    
    2018.12.31
    """
    Er = (E_gamma**2)*(1-np.cos(th))/(C['mc2'] + E_gamma*(1-np.cos(th)))
    return Er

def Compton_Edge(E_gamma):
    """
    Calculates the Compton Edge energy for a gamma ray with energy E_gamma.
    E_gamma should be given in units of keV.
    
    2018.12.31
    """
    Ece = Compton_Energy(E_gamma, np.pi)
    return Ece

def Compton_Scatter(Er, Eg):
    """
    Calculates the differential Compton cross section, given an array of recoil energies (Er)
    and a single gamma energy (Eg).  Energies are in keV, cross section is in cm^2/keV
    
    2018.12.31
    """
    # Convert Er and Eg from keV to J
    Er2 = (Er * 1000) * C['e']
    Eg2 = (Eg * 1000) * C['e']
    Pg = 1 - Er2/Eg2
    cs = 1 - (C['m']*(C['c']**2)/Eg2)*( (1/Pg) - 1.)
    
    sig = .5*(C['alph']**2)*(C['r']**2)*(Pg**2)*( Pg+ (1./Pg) -1 + (cs**2) )
    sig = sig * C['m']*(C['c']**2)/((Er2-Eg2)**2) # convert from m^2/s.r. to m^2 / J
    
    sig = sig * (100**2)*(1.6e-19)*1000 # convert from m^2/J to cm^/keV
    
    sig[Er>Compton_Edge(Eg)] = 0
    return sig


