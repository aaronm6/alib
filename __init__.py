"""
This is aLib.
aLib's development state is "unstable".  I will be changing things and pushing to gitlab.

aLib developed as I migrated from Matlab to Pylab (i.e. numpy+scipy+matplotlib) and created
a set of functions and classes to make analysis simpler for myself.  I've since added to it
here and there.

For a long time, I had tried to make aLib usable in both python2.7 and python3.x.  But I don't
think that makes sense anymore, as 2.7 is officially no longer supported.  So I have stopped
making this attempt: use aLib only with python3.x.

Instead of exhaustively trying to document all of the features of aLib in this docstring, I 
defer to the different modules and packages contained within: each one has fairly comprehensive
documentation.
"""

import sys as _sys
pyVersion = _sys.version_info.major

import numpy as np
import matplotlib as mpl
assert pyVersion >= 3, "Please upgrade to Python version >=3"

from .S import S                    # Subclass of dict; emulates behavior of Matlab structs
from . import mfile                 # Functionality for reading/writing Matlab files
from . import fits                  # Curve-fitting classes
from .misc import *                 # 'misc' is pretty much that... a bunch of random useful functions
from . import astats                # some statistics stuff
from . import rates                 # Modules for WIMP rates and CNNS rates
from . import mathops as mo         # Kind of like 'misc', but math-related
from . import yellin as yl          # Functions for using Yellin's limit-setting methods
from . import physics as ph         # 'misc' for physics-related stuff
from pydoc import pipepager as pp   # pipepager lets you pipe stuff to a function, like the bash |

